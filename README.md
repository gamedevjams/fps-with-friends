# FPS with Friends
#### Aim
I'll try to implement a multiplayer FPS to be playes with friends online or in LAN.

I'll use eNet, for the first time.

I'd like to not use PhysX but implement simple collision detection and resolution,
with Unity Mathematics package, so that I can rely on reproducibility and implement rollback.
I'll need to keep physics simulation to a bare minimum, but I hope that responsiveness and lag will benefit from this.

## Videos
[Floor Generator](https://twitter.com/theGiallo/status/1248337756125831169)
