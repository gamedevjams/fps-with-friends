using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using System;

namespace tg.physics
{
	using static Colliders;

	[ExecuteAlways]
	public class TestCollidersRenderer : MonoBehaviour
	{
		public CollidersRenderer colliders_renderer;
		public World world;
		public Transform rb0_transform;
		public Transform rb1_transform;
		public Rigidbody rb0;
		public Rigidbody rb1;
		void Start()
		{
			tg.log( "TestCollidersRenderer.Start()" );
			world = new World();

			rb0 = world.rigidbodies.new_rigidbody();

			//rb0.add_sphere( new Sphere(){ pos = float3(1,0,0), radius = 1 } );

			//rb0.add_box( new Box(){ center      = float3(3,0,0),
			//                        dir_forward = float3(0,0,1),
			//                        dir_right   = float3(1,0,0),
			//                        dir_up      = float3(0,1,0),
			//                        half_size   = float3(3,0.5f,5)} );

			//rb0.add_capsule( new Capsule(){ a_pos  = float3(-4,0,0),
			//                                ab_dir = normalize(float3(0.5f,0.866f,0)),
			//                                ab_len = 4,
			//                                radius = 0.5f
			//});

			rb0.add_aabb( new AABB(){ min = float3(3,0,0), max = float3( 5, 3, 1 ) } );



			rb1 = world.rigidbodies.new_rigidbody();

			//rb1.add_sphere( new Sphere(){ pos = float3(1,0,0), radius = 2 } );

			//rb1.add_capsule( new Capsule(){ a_pos  = float3(-4,0,0),
			//                                ab_dir = normalize(float3(0.5f,0.866f,0)),
			//                                ab_len = 2,
			//                                radius = 1.5f
			//});

			//rb1.add_box( new Box(){ center      = float3(3,0,0),
			//                        dir_forward = float3(0,0,1),
			//                        dir_right   = float3(1,0,0),
			//                        dir_up      = float3(0,1,0),
			//                        half_size   = float3(3,0.5f,5)} );

			rb1.add_aabb( new AABB(){ min = float3(3,0,0), max = float3( 5, 3, 1 ) } );
		}
		private void Update()
		{
			rb0.SyncTransformIn( rb0_transform );
			rb1.SyncTransformIn( rb1_transform );
			world.step(0);
			rb0.SyncTransformOut( rb0_transform );
			rb1.SyncTransformOut( rb1_transform );

			debug_distances();
		}

		#if UNITY_EDITOR
		public bool initialized => world != null;
		private void init_check()
		{
			if ( ! initialized )
			{
				Start();
			}
		}
		#endif

		private void OnRenderObject()
		{
			#if UNITY_EDITOR
			init_check();
			#endif
			colliders_renderer.Render( world );
		}

		private Tr_Matrix<Distance> dist_tm = new Tr_Matrix<Distance>();
		private void debug_distances()
		{
			shortest_distances( new tg.Array_Slice<AABB>( world.aabbs.array, 0, world.aabbs.count ), dist_tm );

			for ( int i = 0; i < dist_tm.elements_count() - 1; ++i )
			{
				for ( int j = i + 1; j < dist_tm.elements_count(); ++j )
				{
					var d = dist_tm[i,j];
					float3 center_A  = float3(0,0,0);
					float3 center_B  = float3(0,0,0);
					for ( int pi = 0; pi != d.area_A.count; ++pi )
					{
						unsafe
						{
							float3 * pa = & d.area_A.point_0;
							float3 * pb = & d.area_B.point_0;
							center_A += *(pa+pi);
							center_B += *(pb+pi);
							Debug.DrawLine( *(pa+pi), *(pb+pi), Color.black );
							//tg.log( $"pa: {*pa} pb: {*pb}" );

							for ( int pj = pi + 1; pj < d.area_A.count; ++pj )
							{
								Action<float3,float3> line = ( float3 s, float3 e )=>{
									int dif = 0;
									bool3 diff = s != e;
									dif += diff.x ? 1 : 0;
									dif += diff.y ? 1 : 0;
									dif += diff.z ? 1 : 0;
									bool diagonal = dif > 1;
									float3 c = float3(!diff);
									Color color = new Color( c.x, c.y, c.z );
									if ( dif < 3 ) Debug.DrawLine( s, e, diagonal ? color : color );
								};
								line( *(pa+pi), *(pa+pj) );
								line( *(pb+pi), *(pb+pj) );
							}
						}
					}
					center_A /= d.area_A.count;
					center_B /= d.area_B.count;
					Debug.DrawLine( center_A, center_A + d.normal_A, Color.magenta );
					Debug.DrawLine( center_B, center_B + d.normal_B, Color.magenta );
				}
			}
		}
	}
}
