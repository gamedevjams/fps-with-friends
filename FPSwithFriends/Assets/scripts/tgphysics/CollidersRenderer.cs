using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using UnityEngine;
using UnityEngine.Rendering;
using static tg.tg;
using Sirenix.OdinInspector;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace tg.physics
{
	[ExecuteAlways]
	public class CollidersRenderer : MonoBehaviour
	{
		public Material material;

		[Header("Meshes")]
		public Mesh sphere_mesh;
		public Mesh box_mesh;
		public Mesh capsule_body_mesh;
		public Mesh capsule_base_mesh;
		public Mesh capsule_top_mesh;


		private const int FLOAT_SIZE = 4;
		private const int STARTING_INSTANCES_BUF_COUNT = 1024;
		[Button]
		public void Awake()
		{
			OnDestroy();

			//tg.log( "CollidersRender Awake" );
			instance_color_id = Shader.PropertyToID( "instance_color" );

			spheres_material  = new Material( material );
			capsules_material = new Material( material );
			boxes_material    = new Material( material );

			cbs_tracking = new Multi_Dictionary<Camera, CommandBuffer>();
			cbs_tracking.init();
		}
		#if UNITY_EDITOR
		private bool initialized => cbs_tracking.dictionary != null;
		private void init_check()
		{
			if ( ! initialized )
			{
				Awake();
			}
		}
		#endif

		public
		void
		Render( World world )
		{
			#if UNITY_EDITOR
			init_check();
			#endif

			int spheres_count  = world.spheres .count;
			var spheres        = world.spheres .array;
			int aabbs_count    = world.aabbs   .count;
			var aabbs          = world.aabbs   .array;
			int boxes_count    = world.boxes   .count;
			var boxes          = world.boxes   .array;
			int capsules_count = world.capsules.count;
			var capsules       = world.capsules.array;

			#if TG_PHYSICS_DEBUG
			var spheres_colliding  = world.spheres .dbg_is_colliding;
			var aabbs_colliding    = world.aabbs   .dbg_is_colliding;
			var boxes_colliding    = world.boxes   .dbg_is_colliding;
			var capsules_colliding = world.capsules.dbg_is_colliding;
			#endif

			// TODO(theGiallo, 2021-10-27): cache
			Matrix4x4[] spheres_matrices       = new Matrix4x4[spheres_count            ];
			Matrix4x4[] boxes_matrices         = new Matrix4x4[aabbs_count + boxes_count];
			Matrix4x4[] capsules_body_matrices = new Matrix4x4[capsules_count           ];
			Matrix4x4[] capsules_top_matrices  = new Matrix4x4[capsules_count           ];
			Matrix4x4[] capsules_base_matrices = new Matrix4x4[capsules_count           ];

			List<Color> spheres_colors  = new List<Color>( spheres_count             );
			List<Color> boxes_colors    = new List<Color>( aabbs_count + boxes_count );
			List<Color> capsules_colors = new List<Color>( capsules_count            );


			check_buf_size( ref sphers_instances_colors_buf  , spheres_count             );
			check_buf_size( ref boxes_instances_colors_buf   , aabbs_count + boxes_count );
			check_buf_size( ref capsules_instances_colors_buf, capsules_count            );


			for ( int i = 0; i != spheres_count; ++i )
			{
				var s = spheres[i];
				float r = s.radius;
				spheres_matrices[i] = Matrix4x4.TRS( s.pos, Quaternion.identity, new Vector3(r,r,r) );

				#if TG_PHYSICS_DEBUG
				Color32 c = spheres_colliding[i] == 1 ? Color.red : Color.green;
				#else
				Color32 c = Color.green;
				#endif
				spheres_colors.Add( c );
			}

			for ( int i = 0; i != aabbs_count; ++i )
			{
				var aabb = aabbs[i];
				float3 pos  = aabb.center;
				float3 hsize = aabb.size * 0.5f;
				boxes_matrices[i] = Matrix4x4.TRS( pos, Quaternion.identity, hsize );

				#if TG_PHYSICS_DEBUG
				Color32 c = aabbs_colliding[i] == 1 ? Color.red : Color.blue;
				#else
				Color32 c = Color.blue;
				#endif
				boxes_colors.Add( c );
			}

			for ( int i = 0; i != boxes_count; ++i )
			{
				var box = boxes[i];
				var m = new Matrix4x4();
				m.SetColumn( 0, float4( box.v_half_right,   0 ) );
				m.SetColumn( 1, float4( box.v_half_up,      0 ) );
				m.SetColumn( 2, float4( box.v_half_forward, 0 ) );
				m.SetColumn( 3, float4( box.center,         1 ) );
				boxes_matrices[aabbs_count + i] = m;

				#if TG_PHYSICS_DEBUG
				Color32 c = boxes_colliding[i] == 1 ? Color.red : Color.cyan;
				#else
				Color32 c = Color.cyan;
				#endif
				boxes_colors.Add( c );
			}

			for ( int i = 0; i != capsules_count; ++i )
			{
				var capsule = capsules[i];
				float3 size_body = float3( capsule.radius, capsule.ab_len / 2, capsule.radius );
				float3 size_caps = float3( capsule.radius, capsule.radius, capsule.radius );
				var rot = Quaternion.FromToRotation( Vector3.up, capsule.ab_dir );
				capsules_body_matrices[i] = Matrix4x4.TRS( capsule.center, rot, size_body );
				capsules_base_matrices[i] = Matrix4x4.TRS( capsule.a_pos,  rot, size_caps );
				capsules_top_matrices [i] = Matrix4x4.TRS( capsule.b_pos,  rot, size_caps );

				Color cc = new Color( 1, 0.4f, 0.1f, 1 );
				#if TG_PHYSICS_DEBUG
				Color32 c = capsules_colliding[i] == 1 ? Color.red : cc;
				#else
				Color32 c = cc;
				#endif
				capsules_colors.Add( c );
			}


			sphers_instances_colors_buf  .SetData( spheres_colors , 0, 0, spheres_colors .Count );
			boxes_instances_colors_buf   .SetData( boxes_colors   , 0, 0, boxes_colors   .Count );
			capsules_instances_colors_buf.SetData( capsules_colors, 0, 0, capsules_colors.Count );

			spheres_material .SetBuffer( instance_color_id, sphers_instances_colors_buf   );
			boxes_material   .SetBuffer( instance_color_id, boxes_instances_colors_buf    );
			capsules_material.SetBuffer( instance_color_id, capsules_instances_colors_buf );


			if ( cb == null )
			{
				cb = new CommandBuffer();
			}

			{
				Camera camera = Camera.current;
				if ( camera != null && ! camera.name.StartsWith( "Preview Scene Camera" ) )
				{
					var cbs = cbs_tracking[camera];
					int count = ( cbs?.Count ?? 0 );
					//tg.log( $"camera {camera.name}, {camera.commandBufferCount} command buffers, removing {count}" );
					for ( int i = 0; i != count; ++i )
					{
						camera.RemoveCommandBuffer( CameraEvent.BeforeForwardAlpha, cbs[i] );
					}
					cbs?.Clear();
					//tg.log( $"camera {camera.name}, {camera.commandBufferCount} command buffers, removed {count}" );
				}
			}


			cb.Clear();
			cb.DrawMeshInstanced( sphere_mesh,       0, spheres_material,  shaderPass:-1, spheres_matrices,       spheres_count             );
			cb.DrawMeshInstanced( capsule_body_mesh, 0, capsules_material, shaderPass:-1, capsules_body_matrices, capsules_count            );
			cb.DrawMeshInstanced( capsule_base_mesh, 0, capsules_material, shaderPass:-1, capsules_base_matrices, capsules_count            );
			cb.DrawMeshInstanced( capsule_top_mesh,  0, capsules_material, shaderPass:-1, capsules_top_matrices,  capsules_count            );
			cb.DrawMeshInstanced( box_mesh,          0, boxes_material,    shaderPass:-1, boxes_matrices,         aabbs_count + boxes_count );

			//Graphics.ExecuteCommandBuffer( cb );


			{
				Camera camera = Camera.current;
				if ( camera != null && ! camera.name.StartsWith( "Preview Scene Camera" ) )
				{
					//tg.log( $"camera {camera.name}, {camera.commandBufferCount} command buffers, adding one" );
					camera.AddCommandBuffer( CameraEvent.BeforeForwardAlpha, cb );
					//tg.log( $"camera {camera.name}, {camera.commandBufferCount} command buffers, added one" );
					if ( camera.commandBufferCount > 1 )
					{
						tg.log_warn( $"camera {camera.name}, {camera.commandBufferCount} command buffers, added one" );
					}
					cbs_tracking.Add( camera, cb );
				}
			}
			//cb.Release();
			//cb = null;
		}
		private Multi_Dictionary<Camera,CommandBuffer> cbs_tracking;
		private void OnDestroy()
		{
			tg.log( "CollidersRenderer.OnDestroy()" );
			remove_cbs();
		}
		private void OnDisable()
		{
			tg.log( "CollidersRenderer.OnDisable()" );
			remove_cbs();
		}

		private int instance_color_id;
		private GraphicsBuffer sphers_instances_colors_buf;
		private GraphicsBuffer boxes_instances_colors_buf;
		private GraphicsBuffer capsules_instances_colors_buf;
		private Material spheres_material, capsules_material, boxes_material;
		private CommandBuffer cb;

		private void remove_cbs()
		{
			if ( cbs_tracking.dictionary != null )
			{
				foreach ( var kv in cbs_tracking.dictionary )
				{
					Camera camera = kv.Key;
					var cbs = kv.Value;
					int cbs_count = ( cbs?.Count ?? 0 );
					try {
						// NOTE(theGiallo): camera could have been destroyed
						int starting_camera_cbs = camera.commandBufferCount;
						//tg.log( $"camera {camera.name}, {camera.commandBufferCount}, command buffers, removing {cbs_count}" );
						for ( int cbi = 0; cbi != cbs_count; ++cbi )
						{
							camera.RemoveCommandBuffer( CameraEvent.BeforeForwardAlpha, cbs[cbi] );
						}
						//tg.log( $"camera {camera.name}, {camera.commandBufferCount} command buffers, removed {cbs_count}" );
						if ( camera.commandBufferCount > 0 )
						{
							tg.log_warn( $"camera {camera.name}, {camera.commandBufferCount} command buffers, should be none, were {starting_camera_cbs}" );
						}
					} catch ( Exception ) {}
					cbs?.Clear();
				}
				cbs_tracking.dictionary.Clear();
			}
			if ( cb != null )
			{
				cb.Release();
				cb = null;
			}
		}
		private void check_buf_size( ref GraphicsBuffer buf, int count )
		{
			int prev_count = STARTING_INSTANCES_BUF_COUNT;
			if ( buf == null || ( prev_count = buf.count ) < count )
			{
				debug_assert( prev_count != 0 );
				buf = new GraphicsBuffer( GraphicsBuffer.Target.Structured, prev_count * 2, FLOAT_SIZE * 4 );
			}
		}
	}
}
