using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.Mathematics.math;
using Unity.Mathematics;
using static tg.tg;
using System.Runtime.CompilerServices;

namespace tg.physics
{
	public static class
	Colliders
	{
		public enum Collider_Type
		{
			Sphere,
			Capsule,
			AABB,
			Box,
			Cylinder
		}

		[Serializable]
		public struct
		Sphere
		{
			public float3 pos;
			public float  radius;
		}
		[Serializable]
		public struct
		Capsule
		{
			// NOTE(theGiallo): we keep direction and size separate to avoid normalizations
			public float3 a_pos;
			public float3 ab_dir;
			public float  ab_len;
			public float  radius;

			public float3 center => a_pos + ab_dir * ( 0.5f * ab_len );
			public float3 b_pos  => a_pos + ab_dir * ab_len;
			public float  height => ab_len + 2.0f * radius;
		}
		[Serializable]
		public struct
		AABB
		{
			public float3 min, max;

			public float3 size   => ( max - min );
			public float3 hsize  => ( max - min ) / 2f;
			public float3 center => ( max + min ) / 2f;
		}
		[Serializable]
		public struct
		Box
		{
			// NOTE(theGiallo): we keep direction and size separate to avoid normalizations
			public float3 center;
			public float3 v_half_up      => half_up * dir_up;
			public float3 v_half_right   => half_right * dir_right;
			public float3 v_half_forward => half_forward * dir_forward;
			public float3 half_size;
			public float  half_up      => half_size.y;
			public float  half_right   => half_size.x;
			public float  half_forward => half_size.z;
			public float3 dir_up;
			public float3 dir_right;
			public float3 dir_forward;

			public Box( AABB aabb )
			{
				this.center      = aabb.center;
				this.half_size   = aabb.hsize;
				this.dir_up      = Vector3.up;
				this.dir_forward = Vector3.forward;
				this.dir_right   = Vector3.right;
			}
		}
		[Serializable]
		public struct
		Cylinder
		{
			// NOTE(theGiallo): we keep direction and size separate to avoid normalizations
			public float3 a_pos;
			public float3 ab_dir;
			public float  ab_len;
			public float  radius;
			public
			float3
			b_pos()
			{
				return a_pos + ab_dir * ab_len;
			}
		}

		public class
		Collisions_Matrix
		{
			private Bit_Vector bv;
			private int _elements_count;
			private int N, M;

			public bool this[ int a, int b ]
			{
				[MethodImpl(MethodImplOptions.AggressiveInlining)]
				get => 1 == bv[index(a,b)];
				[MethodImpl(MethodImplOptions.AggressiveInlining)]
				set => bv[index(a,b)] = value ? 1 : 0;
			}

			/*
			NOTE(theGiallo): how we store data

			  012345M
			 0######
			 1######
			 2######
			 3######
			 N

			 #: represented
			 ~: represented in inverse indexing
			 -: not represented
			 */

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public
			int
			index( int a, int b ) => a * M + b;

			public
			void
			elements_count( int N, int M )
			{
				int nb = needed_bits( N, M );
				if ( bv.bits_count < nb )
				{
					bv = new Bit_Vector( nb, round_up_count: true );
				}
				_elements_count = nb;
				this.N = N;
				this.M = M;
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public
			int
			needed_bits( int N, int M ) => N * M;
		}

		public class
		Collisions_Tr_Matrix
		{
			private Bit_Vector bv;
			private int bits_count;
			private int _elements_count;

			public bool this[ int a, int b ]
			{
				[MethodImpl(MethodImplOptions.AggressiveInlining)]
				get => 1 == bv[index(a,b)];
				[MethodImpl(MethodImplOptions.AggressiveInlining)]
				set => bv[index(a,b)] = value ? 1 : 0;
			}

			/*
			NOTE(theGiallo): how we store data
			  0123
			 0-###
			 1~-##
			 2~~-#
			 3~~~-
			 #: represented
			 ~: represented in inverse indexing
			 -: not represented
			 */

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public
			int
			index( int a, int b )
			{
				if ( a == b )
				{
					return -1;
				}

				if ( a > b )
				{
					int tmp = a;
					      a = b;
					      b = tmp;
				}

				int before = bits_count - needed_bits( _elements_count - a );
				int this_row_i = b - a - 1;
				int ret = before + this_row_i;
				return ret;
			}

			public
			void
			elements_count( int count )
			{
				int nb = needed_bits( count );
				if ( bv.bits_count < nb )
				{
					bv = new Bit_Vector( nb, round_up_count: true );
				}
				_elements_count = count;
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public
			int
			needed_bits( int count ) => count * ( count - 1 ) / 2;
		}

		public class
		Tr_Matrix<T>
		{
			private T[] array;
			private int _elements_count;
			private int _used_length;

			public ref T this[ int a, int b ]
			{
				[MethodImpl(MethodImplOptions.AggressiveInlining)]
				get => ref array[index(a,b)];
			}

			/*
			NOTE(theGiallo): how we store data
			  0123
			 0-###
			 1~-##
			 2~~-#
			 3~~~-
			 #: represented
			 ~: represented in inverse indexing
			 -: not represented
			 */

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public
			int
			index( int a, int b )
			{
				if ( a == b )
				{
					return -1;
				}

				if ( a > b )
				{
					int tmp = a;
					      a = b;
					      b = tmp;
				}

				int before = _used_length - needed_length( _elements_count - a );
				int this_row_i = b - a - 1;
				int ret = before + this_row_i;
				return ret;
			}

			public
			int
			elements_count() => _elements_count;
			public
			void
			elements_count( int count )
			{
				int nl = needed_length( count );
				if ( array == null || array.Length < nl )
				{
					array = new T[nl];
				}
				_used_length = nl;
				_elements_count = count;
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public
			int
			needed_length( int count ) => count * ( count - 1 ) / 2;
		}



		public static
		int
		collisions( Array_Slice<Sphere> spheres, Collisions_Tr_Matrix cm )
		{
			int ret = 0;

			cm.elements_count( spheres.count );

			for ( int ia = 0; ia < spheres.count - 1; ++ia )
			{
				ref var a = ref spheres.array[spheres.offset + ia];
				for ( int ib = ia + 1; ib < spheres.count; ++ib )
				{
					ref var b = ref spheres.array[spheres.offset + ib];

					bool c = distance( a.pos, b.pos ) <= a.radius + b.radius;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}

			return ret;
		}
		public static
		int
		collisions( Array_Slice<Sphere> spheres, Array_Slice<AABB> aabbs, Collisions_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( spheres.count, aabbs.count );

			for ( int ia = 0; ia < spheres.count; ++ia )
			{
				ref var a = ref spheres.array[spheres.offset + ia];

				for ( int ib = 0; ib < aabbs.count; ++ib )
				{
					ref var b = ref aabbs.array[aabbs.offset + ib];

					float3 bc = ( b.min + b.max ) * 0.5f;
					float3 br = ( b.max - b.min ) * 0.5f;
					float3 p = a.pos - bc;
					float3 q = abs( p ) - br;
					float d = length( max( q, 0 ) ) + min( max( q.x, max( q.y, q.z ) ), 0 );

					bool c = d <= a.radius;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<AABB> aabbs, Collisions_Tr_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( aabbs.count );

			for ( int ia = 0; ia < aabbs.count - 1; ++ia )
			{
				ref var a = ref aabbs.array[aabbs.offset + ia];

				for ( int ib = 0; ib < aabbs.count; ++ib )
				{
					ref var b = ref aabbs.array[aabbs.offset + ib];

					float3 bhmin = b.min * 0.5f;
					float3 bhmax = b.max * 0.5f;
					float3 bc = bhmin + bhmax;
					float3 br = bhmax - bhmin;
					float3 ahmin = a.min * 0.5f;
					float3 ahmax = a.max * 0.5f;
					float3 ac = ahmin + ahmax;
					float3 ar = ahmax - ahmin;
					float3 p = ac - bc;
					float d = length( max( abs( p ) - br - ar, 0 ) );

					bool c = d <= 0;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<Sphere> spheres, Array_Slice<Box> boxes, Collisions_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( spheres.count, boxes.count );

			for ( int ia = 0; ia < spheres.count; ++ia )
			{
				ref var a = ref spheres.array[spheres.offset + ia];

				for ( int ib = 0; ib < boxes.count; ++ib )
				{
					ref var b = ref boxes.array[boxes.offset + ib];


					float3 bc = b.center;
					float3 rp = a.pos - bc;
					float3 p;
					p.x = dot( b.dir_right  , rp );
					p.y = dot( b.dir_up     , rp );
					p.z = dot( b.dir_forward, rp );
					float3 q = abs( p ) - b.half_size;
					float d = length( max( q, 0 ) ) + min( max( q.x, max( q.y, q.z ) ), 0 );

					bool c = d <= a.radius;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<Box> boxes, Collisions_Tr_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( boxes.count );

			for ( int ia = 0; ia < boxes.count - 1; ++ia )
			{
				ref var a = ref boxes.array[boxes.offset + ia];

				for ( int ib = 0; ib < boxes.count; ++ib )
				{
					ref var b = ref boxes.array[boxes.offset + ib];

					float3[] axes = new float3[15]
					{
						a.dir_forward,
						a.dir_right,
						a.dir_up,
						b.dir_forward,
						b.dir_right,
						b.dir_up,
						cross( a.dir_forward, b.dir_forward ),
						cross( a.dir_right,   b.dir_forward ),
						cross( a.dir_up,      b.dir_forward ),
						cross( a.dir_forward, b.dir_right   ),
						cross( a.dir_right,   b.dir_right   ),
						cross( a.dir_up,      b.dir_right   ),
						cross( a.dir_forward, b.dir_up      ),
						cross( a.dir_right,   b.dir_up      ),
						cross( a.dir_up,      b.dir_up      ),
					};

					bool overlap = true;

					for ( int axes_i = 0; axes_i != axes.Length; ++axes_i )
					{
						float3 axis = axes[axes_i];
						float pa = abs( dot( axis, a.v_half_forward ) )
						         + abs( dot( axis, a.v_half_right   ) )
						         + abs( dot( axis, a.v_half_up      ) );
						float pb = abs( dot( axis, b.v_half_forward ) )
						         + abs( dot( axis, b.v_half_right   ) )
						         + abs( dot( axis, b.v_half_up      ) );
						float3 c = b.center - a.center;
						float d = abs( dot( c, axis ) );
						// NOTE(theGiallo): if they touch we return true, hence <= and not <
						overlap = overlap && d <= pa + pb;
					}

					cm[ia,ib] = overlap;
					ret += overlap ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<AABB> aabbs, Array_Slice<Box> boxes, Collisions_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( aabbs.count, boxes.count );

			for ( int ia = 0; ia < aabbs.count; ++ia )
			{
				var a = new Box( aabbs.array[aabbs.offset + ia] );

				for ( int ib = 0; ib < boxes.count; ++ib )
				{
					ref var b = ref boxes.array[boxes.offset + ib];

					float3[] axes = new float3[15]
					{
						Vector3.forward,
						Vector3.right,
						Vector3.up,
						b.dir_forward,
						b.dir_right,
						b.dir_up,
						// TODO(theGiallo, 2021-11-06): optimize these using the known zeroes
						cross( Vector3.forward, b.dir_forward ),
						cross( Vector3.right,   b.dir_forward ),
						cross( Vector3.up,      b.dir_forward ),
						cross( Vector3.forward, b.dir_right   ),
						cross( Vector3.right,   b.dir_right   ),
						cross( Vector3.up,      b.dir_right   ),
						cross( Vector3.forward, b.dir_up      ),
						cross( Vector3.right,   b.dir_up      ),
						cross( Vector3.up,      b.dir_up      ),
					};

					bool overlap = true;

					for ( int axes_i = 0; axes_i != axes.Length; ++axes_i )
					{
						float3 axis = axes[axes_i];
						float pa = abs( dot( axis, a.v_half_forward ) )
						         + abs( dot( axis, a.v_half_right   ) )
						         + abs( dot( axis, a.v_half_up      ) );
						float pb = abs( dot( axis, b.v_half_forward ) )
						         + abs( dot( axis, b.v_half_right   ) )
						         + abs( dot( axis, b.v_half_up      ) );
						float3 c = b.center - a.center;
						float d = abs( dot( c, axis ) );
						// NOTE(theGiallo): if they touch we return true, hence <= and not <
						overlap = overlap && d <= pa + pb;
					}

					cm[ia,ib] = overlap;
					ret += overlap ? 1 : 0;

				}
			}
			return ret;
		}

		public static
		int
		collisions( Array_Slice<Sphere> spheres, Array_Slice<Capsule> capsules, Collisions_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( spheres.count, capsules.count );

			for ( int ia = 0; ia < spheres.count; ++ia )
			{
				ref var a = ref spheres.array[spheres.offset + ia];

				for ( int ib = 0; ib < capsules.count; ++ib )
				{
					ref var b = ref capsules.array[capsules.offset + ib];

					float3 pa = a.pos - b.a_pos;
					float3 ba = - b.ab_dir * b.ab_len;
					float ba_len2 = b.ab_len * b.ab_len;
					float h = clamp( dot( pa, ba ) / ba_len2, 0.0f, 1.0f );
					float3 pabah = pa - ba * h;
					float d2 = dot( pabah, pabah );
					//float d = length( pa - ba * h ) - b.radius;
					float radii = a.radius + b.radius;
					float radii2 = radii * radii;

					bool c = d2 <= radii2;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<AABB> aabbs, Array_Slice<Capsule> capsules, Collisions_Matrix cm )
		// NOTE(theGiallo): we could use the separating axis theorem
		{
			int ret = 0;
			cm.elements_count( aabbs.count, capsules.count );

			for ( int ia = 0; ia < aabbs.count; ++ia )
			{
				ref var aabb = ref aabbs.array[aabbs.offset + ia];

				for ( int ib = 0; ib < capsules.count; ++ib )
				{
					ref var capsule = ref capsules.array[capsules.offset + ib];

					float3 bc = ( aabb.min + aabb.max ) * 0.5f;
					float3 br = ( aabb.max - aabb.min ) * 0.5f;

					float3 sc; // NOTE(theGiallo): sphere center
					float total_distance = 0;
					bool collide = false;
					sc = capsule.a_pos;
					while ( total_distance < capsule.ab_len )
					{
						float3 p = sc - bc;
						float3 q = abs( p ) - br;
						float d = length( max( q, 0 ) ) + min( max( q.x, max( q.y, q.z ) ), 0 );

						d = d - capsule.radius;

						if ( d <= 0 )
						{
							collide = true;
						}
						total_distance += d;
						total_distance = min( total_distance, capsule.ab_len );
						sc = capsule.a_pos + capsule.ab_dir * total_distance;
					}

					cm[ia,ib] = collide;
					ret += collide ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<Box> boxes, Array_Slice<Capsule> capsules, Collisions_Matrix cm )
		// TODO(theGiallo, 2021-11-06): we could use the separating axis theorem
		{
			int ret = 0;
			cm.elements_count( boxes.count, capsules.count );

			for ( int ia = 0; ia < boxes.count; ++ia )
			{
				ref var box = ref boxes.array[boxes.offset + ia];

				for ( int ib = 0; ib < capsules.count; ++ib )
				{
					ref var capsule = ref capsules.array[capsules.offset + ib];

					float total_distance = 0;
					bool collide = false;
					const int MAX_ITERATTIONS_COUNT = 1000;
					int iter_count;
					#if false
					float3 sc; // NOTE(theGiallo): sphere center
					sc = capsule.a_pos;
					for ( iter_count = 0; iter_count < MAX_ITERATTIONS_COUNT && total_distance < capsule.ab_len && ! collide; ++iter_count )
					{
						float3 ac = box.center;
						float3 rp = sc - ac;
						float3 p;
						p.x = dot( box.dir_right  , rp );
						p.y = dot( box.dir_up     , rp );
						p.z = dot( box.dir_forward, rp );
						float3 q = abs( p ) - box.half_size;
						float d = length( max( q, 0 ) ) + min( max( q.x, max( q.y, q.z ) ), 0 );

						d = d - capsule.radius;
						//tg.log( $"[{iter_count:000}] d {d}" );

						if ( d <= 0 )
						{
							collide = true;
						}
						d = max( 0, d );
						total_distance += d;
						total_distance = min( total_distance, capsule.ab_len );
						sc = capsule.a_pos + capsule.ab_dir * total_distance;
					}
					//tg.log( $"[{ia} {ib}] {iter_count} iterations collide {collide} total distance:{total_distance} ab_len:{capsule.ab_len}" );

					#else

					float3 scA = capsule.a_pos;
					float3 b_pos = capsule.b_pos;
					float3 scB = b_pos;
					float total_distance_A = 0, total_distance_B = 0;
					total_distance = 0;
					for ( iter_count = 0; iter_count < MAX_ITERATTIONS_COUNT && total_distance < capsule.ab_len && ! collide; ++iter_count )
					{
						float3 ac = box.center;
						float3 rpA = scA - ac;
						float3 rpB = scB - ac;
						float3 pA, pB;

						pA.x = dot( box.dir_right  , rpA );
						pA.y = dot( box.dir_up     , rpA );
						pA.z = dot( box.dir_forward, rpA );

						pB.x = dot( box.dir_right  , rpB );
						pB.y = dot( box.dir_up     , rpB );
						pB.z = dot( box.dir_forward, rpB );

						float3 qA = abs( pA ) - box.half_size;
						float3 qB = abs( pB ) - box.half_size;
						float dA = length( max( qA, 0 ) ) + min( max( qA.x, max( qA.y, qA.z ) ), 0 );
						float dB = length( max( qB, 0 ) ) + min( max( qB.x, max( qB.y, qB.z ) ), 0 );

						dA = dA - capsule.radius;
						dB = dB - capsule.radius;
						//tg.log( $"[{iter_count:000}] dA {dA} dB {dB}" );

						if ( dA <= 0 || dB <= 0 )
						{
							collide = true;
						}
						dA = max( 0, dA );
						dB = max( 0, dB );
						total_distance_A += dA;
						total_distance_B += dB;
						total_distance = min( total_distance_A + total_distance_B, capsule.ab_len );
						scA = capsule.a_pos + capsule.ab_dir * total_distance_A;
						scB = b_pos - capsule.ab_dir * total_distance_B;
					}
					//tg.log( $"[{ia} {ib}] {iter_count} iterations collide {collide} total distance:{total_distance:E} ab_len:{capsule.ab_len}" );
					#endif

					cm[ia,ib] = collide;
					ret += collide ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<Capsule> capsules, Collisions_Tr_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( capsules.count );

			for ( int ia = 0; ia < capsules.count - 1; ++ia )
			{
				ref var a = ref capsules.array[capsules.offset + ia];

				for ( int ib = 0; ib < capsules.count; ++ib )
				{
					ref var b = ref capsules.array[capsules.offset + ib];

					// NOTE(theGiallo): distance between two segments
					float3 X = float3( -a.ab_dir.y, a.ab_dir.x, 0 );
					float3 Y = cross( a.ab_dir, X );
					float3 rel_bA       = b.a_pos - a.a_pos;
					float3 rel_bB       = b.b_pos - a.a_pos;
					float2 flat_bA      = float2( dot( X, rel_bA ), dot(Y, rel_bA) );
					float2 flat_bB      = float2( dot( X, rel_bB ), dot(Y, rel_bB) );
					float2 flat_bAB     = flat_bB - flat_bA;
					float2 flat_bAB_dir = normalizesafe( flat_bAB );
					float2 flat_N       = float2( -flat_bAB_dir.y, flat_bAB_dir.x );
					float2 flat_bAO     = flat_N * dot( flat_bA, flat_N );
					float2 flat_bAs     = flat_bA - flat_bAO;
					//float2 flat_bBs = flat_bB - flat_bAO;
					float tb = 0;
					if ( flat_bAB.x != 0 )
					{
						tb = - flat_bAs.x / flat_bAB.x;
					} else
					if ( flat_bAB.y != 0 )
					{
						tb = - flat_bAs.y / flat_bAB.y;
					}
					tb = clamp( tb, 0, 1 );
					float3 bP = b.a_pos + b.ab_dir * tb;
					float ta = dot( a.ab_dir, bP - a.a_pos );
					ta = clamp( ta, 0, a.ab_len );
					float3 aP = a.a_pos + a.ab_dir * ta;
					float3 aPbP = bP - aP;
					float d2     = dot( aPbP, aPbP );
					float radii  = a.radius + b.radius;
					float radii2 = radii * radii;

					bool c = d2 <= radii2;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}
			return ret;
		}
		public static
		int
		collisions( Array_Slice<Sphere> spheres, Array_Slice<Cylinder> cylinders, Collisions_Matrix cm )
		{
			int ret = 0;
			cm.elements_count( spheres.count, cylinders.count );

			for ( int ia = 0; ia < spheres.count; ++ia )
			{
				ref var a = ref spheres.array[spheres.offset + ia];

				for ( int ib = 0; ib < cylinders.count; ++ib )
				{
					ref var b = ref cylinders.array[cylinders.offset + ib];

					float3 pa = a.pos - b.a_pos;
					float3 ba = -b.ab_dir * b.ab_len;
					float baba = b.ab_len * b.ab_len;
					float paba = dot( pa, ba );
					float x    = length( pa * baba - ba * paba ) - b.radius * baba;
					float y    = abs( paba - baba * 0.5f ) - baba * 0.5f;
					float x2   = x * x;
					float y2   = y * y * baba;
					float d    = ( max( x, y ) < 0.0f ) ? -min( x2, y2 ) : ( ( ( x > 0.0f ) ? x2 : 0.0f ) + ( ( y > 0.0f ) ? y2 : 0.0f ) );
					d = sign( d ) * sqrt( abs( d ) ) / baba;

					bool c = d <= a.radius;
					cm[ia,ib] = c;
					ret += c ? 1 : 0;
				}
			}
			return ret;
		}
		public interface ICollision_Checker_T1_T2<T1,T2>
		{
			int
			collisions( Array_Slice<T1> colliders_t1, Array_Slice<T2> colliders_t2, Collisions_Matrix cm );
		}
		public interface ICollision_Checker_T1_T1<T1>
		{
			int
			collisions( Array_Slice<T1> colliders_t1, Collisions_Tr_Matrix ctm );
		}
		public class Collision_Checker_Spheres_Spheres : ICollision_Checker_T1_T1<Sphere>
		{
			public int collisions( Array_Slice<Sphere> colliders_t1, Collisions_Tr_Matrix ctm ) => Colliders.collisions( colliders_t1, ctm );
		}
		public class Collision_Checker_AABBs_AABBs : ICollision_Checker_T1_T1<AABB>
		{
			public int collisions( Array_Slice<AABB> colliders_t1, Collisions_Tr_Matrix ctm ) => Colliders.collisions( colliders_t1, ctm );
		}
		public class Collision_Checker_Boxes_Boxes : ICollision_Checker_T1_T1<Box>
		{
			public int collisions( Array_Slice<Box> colliders_t1, Collisions_Tr_Matrix ctm ) => Colliders.collisions( colliders_t1, ctm );
		}
		public class Collision_Checker_Capsules_Capsules : ICollision_Checker_T1_T1<Capsule>
		{
			public int collisions( Array_Slice<Capsule> colliders_t1, Collisions_Tr_Matrix ctm ) => Colliders.collisions( colliders_t1, ctm );
		}
		public class Collision_Checker_Spheres_AABBs : ICollision_Checker_T1_T2<Sphere,AABB>
		{
			public int collisions( Array_Slice<Sphere> colliders_t1, Array_Slice<AABB> colliders_t2, Collisions_Matrix cm ) => Colliders.collisions( colliders_t1, colliders_t2, cm );
		}
		public class Collision_Checker_Spheres_Boxes : ICollision_Checker_T1_T2<Sphere,Box>
		{
			public int collisions( Array_Slice<Sphere> colliders_t1, Array_Slice<Box> colliders_t2, Collisions_Matrix cm ) => Colliders.collisions( colliders_t1, colliders_t2, cm );
		}
		public class Collision_Checker_Spheres_Capsules : ICollision_Checker_T1_T2<Sphere,Capsule>
		{
			public int collisions( Array_Slice<Sphere> colliders_t1, Array_Slice<Capsule> colliders_t2, Collisions_Matrix cm ) => Colliders.collisions( colliders_t1, colliders_t2, cm );
		}
		public class Collision_Checker_AABBs_Boxes : ICollision_Checker_T1_T2<AABB,Box>
		{
			public int collisions( Array_Slice<AABB> colliders_t1, Array_Slice<Box> colliders_t2, Collisions_Matrix cm ) => Colliders.collisions( colliders_t1, colliders_t2, cm );
		}
		public class Collision_Checker_AABBs_Capsules : ICollision_Checker_T1_T2<AABB,Capsule>
		{
			public int collisions( Array_Slice<AABB> colliders_t1, Array_Slice<Capsule> colliders_t2, Collisions_Matrix cm ) => Colliders.collisions( colliders_t1, colliders_t2, cm );
		}
		public class Collision_Checker_Boxes_Capsules : ICollision_Checker_T1_T2<Box,Capsule>
		{
			public int collisions( Array_Slice<Box> colliders_t1, Array_Slice<Capsule> colliders_t2, Collisions_Matrix cm ) => Colliders.collisions( colliders_t1, colliders_t2, cm );
		}


		public struct
		Distance
		{
			public
			struct
			Area
			{
				public float3 point_0;
				public float3 point_1;
				public float3 point_2;
				public float3 point_3;
				// NOTE(theGiallo): if there are 8 points, it's a volume
				public float3 point_4;
				public float3 point_5;
				public float3 point_6;
				public float3 point_7;
				public int count;
			}
			public float  distance;
			public Area   area_A;
			public Area   area_B;
			public float3 normal_A;
			public float3 normal_B;
		}
		public static
		void
		shortest_distances( Array_Slice<Sphere> spheres, Tr_Matrix<Distance> dist_tm )
		{
			dist_tm.elements_count( spheres.count );

			for ( int ia = 0; ia < spheres.count - 1; ++ia )
			{
				ref var a = ref spheres.array[spheres.offset + ia];
				for ( int ib = ia + 1; ib < spheres.count; ++ib )
				{
					ref var b = ref spheres.array[spheres.offset + ib];

					float3 ab_dir = b.pos - a.pos;
					float dist = length( ab_dir );
					if ( dist != 0 )
					{
						ab_dir /= dist;
					}
					float radii = a.radius + b.radius;
					dist -= radii;
					Distance d = new Distance(){
					   distance = dist,
					   area_A = new Distance.Area{ point_0  = a.pos + ab_dir * a.radius, count = 1 },
					   area_B = new Distance.Area{ point_0  = b.pos - ab_dir * b.radius, count = 1 },
					   normal_A = ab_dir,
					   normal_B = -ab_dir,
					};
					dist_tm[ia,ib] = d;
				}
			}
		}
		public static
		void
		shortest_distances( Array_Slice<AABB> aabbs, Tr_Matrix<Distance> dist_tm )
		{
			dist_tm.elements_count( aabbs.count );

			for ( int ia = 0; ia < aabbs.count - 1; ++ia )
			{
				ref var a = ref aabbs.array[aabbs.offset + ia];
				for ( int ib = ia + 1; ib < aabbs.count; ++ib )
				{
					ref var b = ref aabbs.array[aabbs.offset + ib];

					float3 pa      = default, pb      = default;
					float3 p_min   = default, p_max   = default;
					float3[][] p_min_max_a_b = new float3[2][]{ new float3[2], new float3[2] };
					float3[] p_min_max_a = p_min_max_a_b[0];
					float3[] p_min_max_b = p_min_max_a_b[1];
					int3[] count_a_b = new int3[2];
					ref int3 count_a = ref count_a_b[0];
					ref int3 count_b = ref count_a_b[1];
					for ( int i = 0; i != 3; ++i )
					{
						if ( a.max[i] <= b.min[i] )
						{
							p_min_max_a[0][i] = pa[i] = a.max[i];
							p_min_max_b[0][i] = pb[i] = b.min[i];
							count_a[i] = count_b[i] = 1;
						} else
						if ( b.max[i] < a.min[i] )
						{
							p_min_max_b[0][i] = pb[i] = b.max[i];
							p_min_max_a[0][i] = pa[i] = a.min[i];
							count_a[i] = count_b[i] = 1;
						} else
						{
							p_min_max_a[0][i] = p_min_max_b[0][i] = p_min[i] = max( a.min[i], b.min[i] );
							p_min_max_a[1][i] = p_min_max_b[1][i] = p_max[i] = min( a.max[i], b.max[i] );
							count_a[i] = count_b[i] = 2;
						}
					}

					// NOTE(theGiallo): if points are 6, it means it's a volume, so there's intersection
					var areas = new Distance.Area[2] {
					   new Distance.Area{ count = count_a.x * count_a.y * count_a.z },
					   new Distance.Area{ count = count_a.x * count_b.y * count_b.z } };
					for ( int abi = 0; abi != 2; ++abi )
					unsafe
					{
						ref int count = ref areas[abi].count;
						count = 0;
						fixed ( float3 * arr = & areas[abi].point_0 )
						for ( int ix = 0; ix != count_a_b[abi][0]; ++ix )
						for ( int iy = 0; iy != count_a_b[abi][1]; ++iy )
						for ( int iz = 0; iz != count_a_b[abi][2]; ++iz )
						{
							float3 p;
							p.x = p_min_max_a_b[abi][ix][0];
							p.y = p_min_max_a_b[abi][iy][1];
							p.z = p_min_max_a_b[abi][iz][2];
							arr[count++] = p;
						}
					}

					// NOTE(theGiallo): being axis aligned, segments of points N - M and N+1 - M+1 are parallel
					float dist = length( areas[0].point_0 - areas[1].point_0 );
					float3 normal_A = normalize( areas[1].point_0 - areas[0].point_0 );
					float3 normal_B = - normal_A;

					Distance d = new Distance(){
					   distance = dist,
					   area_A   = areas[0],
					   area_B   = areas[1],
					   normal_A = normal_A,
					   normal_B = normal_B,
					};
					dist_tm[ia,ib] = d;
				}
			}
		}
	}
}
