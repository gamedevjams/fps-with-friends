using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Mathematics;
using Quaternion = UnityEngine.Quaternion;
using static Unity.Mathematics.math;
using static tg.tg;

namespace tg.physics
{
	using static Colliders;
	using static World;
	using quat = Unity.Mathematics.quaternion;
	public class Rigidbody
	{
		public float mass
		{
			get => 1 / inverseMass;
			set => inverseMass = 1 / value;
		}
		public float inverseMass;

		public Rigidbody_ID id;
		public float3       pos;
		public quat         rot = quat.identity;

		public Rigidbody() {}

		public event Action<Collision_Event> on_collision_event;

		public
		void
		_handle_collision_event( Collision_Event ce )
		{
			on_collision_event?.Invoke( ce );
		}

		public
		void
		SetPos( UnityEngine.Transform tr )
		{
			pos = tr.position;
		}
		public
		void
		SyncTransformIn( UnityEngine.Transform tr )
		{
			float3 p = tr.position;
			quat   r = tr.rotation;

			float3 pivot = pos;

			quat drot = mul( r, inverse( rot ) );

			float3 dpos = p - pos;

			colliders.transform( world, pivot, drot, dpos );

			pos = p;
			rot = r;
		}
		public
		void
		SyncTransformOut( UnityEngine.Transform tr )
		{
			tr.position = pos;
			tr.rotation = rot;
		}

		public World     world;
		public Colliders colliders = new Colliders();

		public Collider_ID add_sphere()                    => world.add_sphere( this );
		public Collider_ID add_sphere( Sphere sphere )     => world.add_sphere( this, sphere );
		public Collider_ID add_aabb()                      => world.add_aabb( this );
		public Collider_ID add_aabb( AABB aabb )           => world.add_aabb( this, aabb );
		public Collider_ID add_box()                       => world.add_box( this );
		public Collider_ID add_box( Box box )              => world.add_box( this, box );
		public Collider_ID add_capsule()                   => world.add_capsule( this );
		public Collider_ID add_capsule( Capsule capsule )  => world.add_capsule( this, capsule );
		public void remove_sphere( Collider_ID cid )       => world.remove_sphere( cid, this );
		public void remove_aabb( Collider_ID cid )         => world.remove_aabb( cid, this );
		public void remove_box( Collider_ID cid )          => world.remove_box( cid, this );
		public void remove_capsule( Collider_ID cid )      => world.remove_capsule( cid, this );
		#if false
		public Collider_ID add_cylinder( Rigidbody rb )                    => world.add_cylinder( this );
		public Collider_ID add_cylinder( Rigidbody rb, Cylinder cylinder ) => world.add_cylinder( this, cylinder );
		public void remove_cylinder( Collider_ID cid )                     => world.remove_cylinder( cid, this );
		#endif



		public
		class
		Colliders
		{
			public List<Collider_ID> id_spheres  = new List<Collider_ID>();
			public List<Collider_ID> id_aabbs    = new List<Collider_ID>();
			public List<Collider_ID> id_boxes    = new List<Collider_ID>();
			public List<Collider_ID> id_capsules = new List<Collider_ID>();

			public
			void
			transform( World world, float3 pivot, quaternion drot, float3 dpos )
			{
				for ( int i = 0; i != id_spheres.Count; ++i )
				{
					var id = id_spheres[i];
					var c = world.spheres.get( id );
					c.pos = rotate( drot, c.pos - pivot ) + pivot + dpos;
					world.spheres.set( id, c );
				}

				for ( int i = 0; i != id_aabbs.Count; ++i )
				{
					var id = id_aabbs[i];
					var c = world.aabbs.get( id );
					float3 center = rotate( drot, c.center - pivot ) + pivot + dpos;
					float3 hsize = c.hsize;
					c.min = center - hsize;
					c.max = center + hsize;
					world.aabbs.set( id, c );
				}

				for ( int i = 0; i != id_boxes.Count; ++i )
				{
					var id = id_boxes[i];
					var c = world.boxes.get( id );
					c.dir_up      = rotate( drot, c.dir_up      );
					c.dir_right   = rotate( drot, c.dir_right   );
					c.dir_forward = rotate( drot, c.dir_forward );
					c.center = rotate( drot, c.center - pivot ) + pivot + dpos;
					world.boxes.set( id, c );
				}

				for ( int i = 0; i != id_capsules.Count; ++i )
				{
					var id = id_capsules[i];
					var c = world.capsules.get( id );
					c.ab_dir = rotate( drot, c.ab_dir      );
					c.a_pos  = rotate( drot, c.a_pos - pivot ) + pivot + dpos;
					world.capsules.set( id, c );
				}

				// TODO(theGiallo, 2021-11-01): implement cylinders
			}
		}
	}
	public class World
	{
		public struct Rigidbody_ID
		{
			public int id;


			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public static bool operator ==(Rigidbody_ID a, Rigidbody_ID b) => a.id == b.id;
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public static bool operator !=(Rigidbody_ID a, Rigidbody_ID b) => a.id != b.id;
			public override string ToString() => $"{id}";
		}
		public struct Collider_ID
		{
			public int id;


			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public static bool operator ==( Collider_ID a, Collider_ID b) => a.id == b.id;
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public static bool operator !=( Collider_ID a, Collider_ID b) => a.id != b.id;
			public override string ToString() => $"{id}";
		}
		public struct Collider_Type_ID
		{
			public Collider_Type type;
			public Collider_ID    id;



			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public static bool operator ==( Collider_Type_ID a, Collider_Type_ID b) => a.type == b.type && a.id == b.id;
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public static bool operator !=( Collider_Type_ID a, Collider_Type_ID b) => a.type != b.type || a.id != b.id;
			public override string ToString() => $"{type}({id})";
		}
		public struct Collision_Event
		{
			public Collider_Type_ID a;
			public Collider_Type_ID b;
			public Rigidbody_ID     owner_a;
			public Rigidbody_ID     owner_b;
			public Collision_Event( Collider_Type_ID a, Collider_Type_ID b, Rigidbody_ID owner_a, Rigidbody_ID owner_b )
			{
				this.a       = a;
				this.b       = b;
				this.owner_a = owner_a;
				this.owner_b = owner_b;
			}
		}

		public Colliders<Sphere > spheres  { get; private set; } = new Colliders<Sphere >();
		public Colliders<AABB   > aabbs    { get; private set; } = new Colliders<AABB   >();
		public Colliders<Box    > boxes    { get; private set; } = new Colliders<Box    >();
		public Colliders<Capsule> capsules { get; private set; } = new Colliders<Capsule>();

		public Rigidbodies rigidbodies { get; private set; }

		public World()
		{
			rigidbodies = new Rigidbodies( this );
		}

		public Collider_ID add_sphere( Rigidbody rb )
		{
			Collider_ID ret = spheres.new_( rb.id );
			rb.colliders.id_spheres.Add( ret );
			return ret;
		}
		public void remove_sphere( Collider_ID cid )
		{
			// TODO(theGiallo 2021-10-30): check for valid index?
			Rigidbody rb = rigidbodies.get( spheres._get_owner_id( spheres._get_index( cid ) ) );
			remove_sphere( cid, rb );
		}
		public void remove_sphere( Collider_ID cid, Rigidbody rb )
		{
			rb.colliders.id_spheres.Remove( cid );
			spheres.remove( cid );
		}
		public Collider_ID add_sphere( Rigidbody rb, Sphere sphere )
		{
			Collider_ID ret = spheres.new_( rb.id, sphere );
			rb.colliders.id_spheres.Add( ret );
			return ret;
		}
		public Collider_ID add_aabb( Rigidbody rb )
		{
			Collider_ID ret = aabbs.new_( rb.id );
			rb.colliders.id_aabbs.Add( ret );
			return ret;
		}
		public Collider_ID add_aabb( Rigidbody rb, AABB aabb )
		{
			Collider_ID ret = aabbs.new_( rb.id, aabb );
			rb.colliders.id_aabbs.Add( ret );
			return ret;
		}
		public void remove_aabb( Collider_ID cid )
		{
			// TODO(theGiallo 2021-10-30): check for valid index?
			Rigidbody rb = rigidbodies.get( aabbs._get_owner_id( aabbs._get_index( cid ) ) );
			remove_aabb( cid, rb );
		}
		public void remove_aabb( Collider_ID cid, Rigidbody rb )
		{
			rb.colliders.id_aabbs.Remove( cid );
			aabbs.remove( cid );
		}
		public Collider_ID add_box( Rigidbody rb )
		{
			Collider_ID ret = boxes.new_( rb.id );
			rb.colliders.id_boxes.Add( ret );
			return ret;
		}
		public Collider_ID add_box( Rigidbody rb, Box box )
		{
			Collider_ID ret = boxes.new_( rb.id, box );
			rb.colliders.id_boxes.Add( ret );
			return ret;
		}
		public void remove_box( Collider_ID cid )
		{
			// TODO(theGiallo 2021-10-30): check for valid index?
			Rigidbody rb = rigidbodies.get( boxes._get_owner_id( boxes._get_index( cid ) ) );
			remove_box( cid, rb );
		}
		public void remove_box( Collider_ID cid, Rigidbody rb )
		{
			rb.colliders.id_boxes.Remove( cid );
			boxes.remove( cid );
		}
		public Collider_ID add_capsule( Rigidbody rb )
		{
			Collider_ID ret = capsules.new_( rb.id );
			rb.colliders.id_capsules.Add( ret );
			return ret;
		}
		public Collider_ID add_capsule( Rigidbody rb, Capsule capsule )
		{
			Collider_ID ret = capsules.new_( rb.id, capsule );
			rb.colliders.id_capsules.Add( ret );
			return ret;
		}
		public void remove_capsule( Collider_ID cid )
		{
			// TODO(theGiallo 2021-10-30): check for valid index?
			Rigidbody rb = rigidbodies.get( capsules._get_owner_id( capsules._get_index( cid ) ) );
			remove_capsule( cid, rb );
		}
		public void remove_capsule( Collider_ID cid, Rigidbody rb )
		{
			rb.colliders.id_capsules.Remove( cid );
			capsules.remove( cid );
		}
		#if false
		public Collider_ID add_cylinder( Rigidbody rb )
		{
			Collider_ID ret = cylinders.new_( rb.id );
			rb.colliders.id_cylinders.Add( ret );
			return ret;
		}
		public Collider_ID add_cylinder( Rigidbody rb, Sphere cylinder )
		{
			Collider_ID ret = cylinders.new_( rb.id, cylinder );
			rb.colliders.id_cylinders.Add( ret );
			return ret;
		}
		public void remove_cylinder( Collider_ID cid )
		{
			// TODO(theGiallo 2021-10-30): check for valid index?
			Rigidbody rb = rigidbodies.get( cylinders._get_owner_id( cylinders._get_index( cid ) ) );
			remove_cylinder( cid, rb );
		}
		public void remove_cylinder( Collider_ID cid, Rigidbody rb )
		{
			rb.colliders.id_cylinders.Remove( cid );
			cylinders.remove( cid );
		}
		#endif

		public
		void
		step( float dt )
		{
			#if TG_PHYSICS_DEBUG
			if ( spheres.dbg_is_colliding.bits_count < spheres.count )
			{
				spheres.dbg_is_colliding = new Bit_Vector( spheres.count );
			}
			spheres.dbg_is_colliding.clear();

			if ( aabbs.dbg_is_colliding.bits_count < aabbs.count )
			{
				aabbs.dbg_is_colliding = new Bit_Vector( aabbs.count );
			}
			aabbs.dbg_is_colliding.clear();

			if ( boxes.dbg_is_colliding.bits_count < boxes.count )
			{
				boxes.dbg_is_colliding = new Bit_Vector( boxes.count );
			}
			boxes.dbg_is_colliding.clear();

			if ( capsules.dbg_is_colliding.bits_count < capsules.count )
			{
				capsules.dbg_is_colliding = new Bit_Vector( capsules.count );
			}
			capsules.dbg_is_colliding.clear();
			#endif

			//spheres_vs_spheres();
			//spheres_vs_aabbs();
			//spheres_vs_boxes();
			//spheres_vs_capsules();
			//aabbs_vs_aabbs();
			//aabbs_vs_boxes();
			//aabbs_vs_capsules();
			//boxes_vs_boxes();
			boxes_vs_capsules();
			//capsules_vs_capsules();

			send_events();
		}

		public void t1_vs_t2<T1,T2>( Colliders<T1> colliders_1,
		                             Colliders<T2> colliders_2,
		                             ICollision_Checker_T1_T2<T1,T2> collision_checker,
		                             Collider_Type t1_type,
		                             Collider_Type t2_type ) where T1 : new() where T2 : new()
		{
			var t1_slice = new Array_Slice<T1>( colliders_1.array, 0, colliders_1.count );
			var t2_slice = new Array_Slice<T2>( colliders_2.array, 0, colliders_2.count );
			int count = collision_checker.collisions( t1_slice, t2_slice, cm );
			if ( count > 0 )
			{
				for ( int ia = 0; ia < t1_slice.count; ++ia )
				{
					//ref var a = ref t1_slice.array[t1_slice.offset + ia];
					for ( int ib = 0; ib < t2_slice.count; ++ib )
					{
						//ref var b = ref t2_slice.array[t2_slice.offset + ia];
						if ( cm[ia,ib] )
						{
							var owner_id_a = colliders_1._get_owner_id( ia );
							var owner_id_b = colliders_2._get_owner_id( ib );
							if ( owner_id_a != owner_id_b )
							{
								#if TG_PHYSICS_DEBUG
								colliders_1.dbg_is_colliding[ia] = 1;
								colliders_2.dbg_is_colliding[ib] = 1;
								#endif
								var id_a = colliders_1._get_id( ia );
								var id_b = colliders_2._get_id( ib );
								new_collision_event( new Collider_Type_ID(){ type = t1_type, id = id_a },
								                     new Collider_Type_ID(){ type = t2_type, id = id_b },
								                     owner_id_a, owner_id_b );
							}
						}
					}
				}
			}
		}
		public void t1_vs_t1<T1>( Colliders<T1> colliders_1,
		                          ICollision_Checker_T1_T1<T1> collision_checker,
		                          Collider_Type t1_type ) where T1 : new()
		{
			var colliders_slice = new Array_Slice<T1>( colliders_1.array, 0, colliders_1.count );
			int count = collision_checker.collisions( colliders_slice, ctm );
			if ( count > 0 )
			{
				for ( int ia = 0; ia < colliders_slice.count - 1; ++ia )
				{
					//ref var a = ref colliders_slice.array[colliders_slice.offset + ia];
					for ( int ib = ia + 1; ib < colliders_slice.count; ++ib )
					{
						if ( ctm[ia,ib] )
						{
							var owner_id_a = colliders_1._get_owner_id( ia );
							var owner_id_b = colliders_1._get_owner_id( ib );
							if ( owner_id_a != owner_id_b )
							{
								#if TG_PHYSICS_DEBUG
								colliders_1.dbg_is_colliding[ia] = 1;
								colliders_1.dbg_is_colliding[ib] = 1;
								#endif
								var id_a = colliders_1._get_id( ia );
								var id_b = colliders_1._get_id( ib );
								new_collision_event( new Collider_Type_ID(){ type = t1_type, id = id_a },
								                     new Collider_Type_ID(){ type = t1_type, id = id_b },
								                     owner_id_a, owner_id_b );
							}
						}
					}
				}
			}
		}

		private void spheres_vs_spheres()   => t1_vs_t1( spheres,  new Collision_Checker_Spheres_Spheres(),   Collider_Type.Sphere  );
		private void aabbs_vs_aabbs()       => t1_vs_t1( aabbs,    new Collision_Checker_AABBs_AABBs(),       Collider_Type.AABB    );
		private void boxes_vs_boxes()       => t1_vs_t1( boxes,    new Collision_Checker_Boxes_Boxes(),       Collider_Type.Box     );
		private void capsules_vs_capsules() => t1_vs_t1( capsules, new Collision_Checker_Capsules_Capsules(), Collider_Type.Capsule );
		private void spheres_vs_aabbs()     => t1_vs_t2( spheres, aabbs,    new Collision_Checker_Spheres_AABBs(),    Collider_Type.Sphere, Collider_Type.AABB    );
		private void spheres_vs_boxes()     => t1_vs_t2( spheres, boxes,    new Collision_Checker_Spheres_Boxes(),    Collider_Type.Sphere, Collider_Type.Box     );
		private void spheres_vs_capsules()  => t1_vs_t2( spheres, capsules, new Collision_Checker_Spheres_Capsules(), Collider_Type.Sphere, Collider_Type.Capsule );
		private void aabbs_vs_boxes()       => t1_vs_t2( aabbs,   boxes,    new Collision_Checker_AABBs_Boxes(),      Collider_Type.AABB,   Collider_Type.Box     );
		private void aabbs_vs_capsules()    => t1_vs_t2( aabbs,   capsules, new Collision_Checker_AABBs_Capsules(),   Collider_Type.AABB,   Collider_Type.Capsule );
		private void boxes_vs_capsules()    => t1_vs_t2( boxes,   capsules, new Collision_Checker_Boxes_Capsules(),   Collider_Type.Box,    Collider_Type.Capsule );

		private Collisions_Tr_Matrix ctm = new Collisions_Tr_Matrix();
		private Collisions_Matrix    cm  = new Collisions_Matrix();
		private class Collision_Events_Queues
		{
			public Dictionary<Rigidbody_ID, Queue<Collision_Event>> dictionary = new Dictionary<Rigidbody_ID, Queue<Collision_Event>>();
			public void Enqueue( Collision_Event ce )
			{
				var qa = get_or_create_queue( ce.owner_a );
				var qb = get_or_create_queue( ce.owner_b );
				qa.Enqueue( ce );
				qb.Enqueue( ce );
			}
			public void Clear()
			{
				dictionary.Clear();
				// TODO(theGiallo, 2021-10-30): pool queues if necessary
			}
			private Queue<Collision_Event> get_or_create_queue( Rigidbody_ID rid )
			{
				if ( ! dictionary.TryGetValue( rid, out Queue<Collision_Event> q ) )
				{
					dictionary.Add( rid, q = new Queue<Collision_Event>() );
				}
				return q;
			}
		}
		private Collision_Events_Queues collision_events_queues = new Collision_Events_Queues();

		private void send_events()
		{
			foreach ( var kv in collision_events_queues.dictionary )
			{
				Rigidbody_ID rid = kv.Key;
				Queue<Collision_Event> q  = kv.Value;
				while ( q.Count > 0 )
				{
					var ce = q.Dequeue();
					Rigidbody rb = rigidbodies.get( rid );
					rb._handle_collision_event( ce );
				}
			}
			collision_events_queues.Clear();
		}

		private void new_collision_event( Collider_Type_ID a, Collider_Type_ID b, Rigidbody_ID owner_a, Rigidbody_ID owner_b )
		{
			//log( $"{owner_a}[{a}] vs {owner_b}[{b}]" );
			var ce = new Collision_Event( a, b,  owner_a, owner_b );
			collision_events_queues.Enqueue( ce );
		}

		public class Rigidbodies
		{
			private List<Rigidbody   > rigidbodies = new List<Rigidbody  >();
			private List<Rigidbody_ID> ids         = new List<Rigidbody_ID>();

			private Pool<Rigidbody> rigidbodies_pool = new Pool<Rigidbody>();
			private Rigidbody_ID next_rigidbodyID = new Rigidbody_ID(){ id = 0 };
			private Dictionary<Rigidbody_ID,int> index_of_id = new Dictionary<Rigidbody_ID, int>();

			private World world;
			public Rigidbodies( World world )
			{
				this.world = world;
			}

			public Rigidbody new_rigidbody()
			{
				Rigidbody ret = rigidbodies_pool.take();
				ret.world = world;
				var id = ret.id = new_rigidbody_id();
				rigidbodies.Add( ret );
				ids        .Add( id );
				index_of_id[id] = rigidbodies.Count - 1;
				return ret;
			}
			public Rigidbody get( Rigidbody_ID id )
			{
				Rigidbody ret = null;
				if ( index_of_id.TryGetValue( id, out int index ) )
				{
					ret = rigidbodies[index];
				}
				return ret;
			}
			public void remove( Rigidbody rb )
			{
				var id = rb.id;
				int index = index_of_id[id];
				int last_i = rigidbodies.Count - 1;
				var moved_id =
				ids        [index] = ids        [last_i];
				rigidbodies[index] = rigidbodies[last_i];
				index_of_id[moved_id] = index;

				rigidbodies.RemoveAt( last_i );
				ids        .RemoveAt( last_i );
				index_of_id.Remove( id );
			}

			private Rigidbody_ID new_rigidbody_id()
			{
				Rigidbody_ID ret = next_rigidbodyID;
				++next_rigidbodyID.id;
				debug_assert( ret.id + 1 == next_rigidbodyID.id );
				return ret;
			}
		}

		public class Colliders<CT> where CT : new()
		{
			private List<CT          > colliders  = new List<CT          >();
			private List<Collider_ID > ids        = new List<Collider_ID >();
			private List<Rigidbody_ID> owners_ids = new List<Rigidbody_ID>();

			#if TG_PHYSICS_DEBUG
			public Bit_Vector dbg_is_colliding = new Bit_Vector(0);
			#endif

			private Collider_ID  next_collider_id = new Collider_ID(){ id = 0 };
			private Dictionary<Collider_ID,int> index_of_collider = new Dictionary<Collider_ID, int>();

			static System.Reflection.FieldInfo list_items_fieldinfo = typeof(List<CT>).GetField( "_items", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance );

			public CT[] array => (CT[])list_items_fieldinfo.GetValue( colliders );
			public int count => colliders.Count;

			public
			Collider_ID
			new_( Rigidbody_ID owner_id )
			{
				Collider_ID ret = new_id();
				CT c = new CT();
				colliders .Add( c );
				ids       .Add( ret );
				owners_ids.Add( owner_id );
				index_of_collider[ret] = colliders.Count - 1;
				return ret;
			}
			public
			Collider_ID
			new_( Rigidbody_ID owner_id, CT val )
			{
				Collider_ID ret = new_id();
				colliders .Add( val );
				ids       .Add( ret );
				owners_ids.Add( owner_id );
				index_of_collider[ret] = colliders.Count - 1;
				return ret;
			}
			public
			void
			set( Collider_ID id, CT v )
			{
				if ( index_of_collider.TryGetValue( id, out int index ) )
				{
					colliders[index] = v;
				}
			}
			public
			CT
			get( Collider_ID id )
			{
				CT ret = default;
				if ( index_of_collider.TryGetValue( id, out int index ) )
				{
					ret = colliders[index];
				}
				return ret;
			}

			public
			int
			_get_index( Collider_ID cid ) => index_of_collider.TryGetValue( cid, out int ret ) ? ret : -1;
			public
			Rigidbody_ID
			_get_owner_id( int index ) => owners_ids[index];
			public
			Collider_ID
			_get_id( int index ) => ids[index];
			public
			CT
			_get( int index ) => colliders[index];

			public
			void
			remove( Collider_ID id )
			{
				if ( index_of_collider.TryGetValue( id, out int index ) )
				{
					int last_i = colliders.Count - 1;

					var moved_id =
					ids       [index] = ids       [last_i];
					colliders [index] = colliders [last_i];
					owners_ids[index] = owners_ids[last_i];

					index_of_collider[ moved_id ] = index;

					index_of_collider.Remove( id );
					colliders .RemoveAt( last_i );
					ids       .RemoveAt( last_i );
					owners_ids.RemoveAt( last_i );
				}
			}

			private
			Collider_ID
			new_id()
			{
				Collider_ID ret = next_collider_id;
				++next_collider_id.id;
				debug_assert( ret.id + 1 == next_collider_id.id );
				return ret;
			}
		}

		// TODO(theGiallo, 2021-10-24): IMPLEMENT cylinders
	}
}