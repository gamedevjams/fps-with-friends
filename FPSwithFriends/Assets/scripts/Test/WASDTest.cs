﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;
using UnityEngine.InputSystem.Controls;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class WASDTest : MonoBehaviour
	{
		public
		enum
		Update_Method
		{
			UNITY,
			RAW
		}
		public CapsuleCollider capsule_collider;
		public Rigidbody       rigidbody;
		public GameObject      collision_marker_prefab;
		public float           v = 4.0f;
		public bool            spawn_debug_arrows;
		public Update_Method   update_method;
		public Text            update_method_text;

		#region private
		private Game_Input.Device_Path mouse_path;
		private bool                   got_mouse_path;
		private Game_Input.Device_Path keyboard_path;
		private bool                   got_keyboard_path;

		private
		void
		new_mouse_h( object caller, Game_Input.Device_Path path )
		{
			if ( got_mouse_path )
			{
				return;
			}
			got_mouse_path = true;
			mouse_path = path;
			Game_Input.instance.on_new_mouse -= new_mouse_h;
		}

		private
		void
		new_keyboard_h( object caller, Game_Input.Device_Path path )
		{
			if ( got_keyboard_path )
			{
				return;
			}
			got_keyboard_path = true;
			keyboard_path = path;
			Game_Input.instance.on_new_keyboard -= new_keyboard_h;
		}
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
			Game_Input.instance.on_new_mouse += new_mouse_h;
			Game_Input.instance.on_new_keyboard += new_keyboard_h;

			update_method_text.text = update_method.ToString();
		}
		private void FixedUpdate()
		{
			if ( update_method == Update_Method.RAW )
			{
				Game_Input.Keyboard_Status k;
				Game_Input.Mouse_Status    m;
				if ( !got_keyboard_path || ! Game_Input.instance.keyboards.TryGetValue( keyboard_path, out k ) )
				{
					log_warn( "no keybaord" );
					k = new Game_Input.Keyboard_Status();
				}
				if ( !got_mouse_path || ! Game_Input.instance.mouses.TryGetValue( mouse_path, out m ) )
				{
					log_warn( "no mouse" );
					m = new Game_Input.Mouse_Status();
				}

				Vector3 nv = Vector3.zero;
				bool moving = false;
				if ( k.key_status_down( Game_Input.Index.w ) )
				{
					nv += new Vector3(v,0,0);
					moving = true;
				}
				if ( k.key_status_down( Game_Input.Index.s ) )
				{
					nv += new Vector3(-v,0,0);
					moving = true;
				}
				if ( k.key_status_down( Game_Input.Index.d ) )
				{
					nv += new Vector3(0,0,-v);
					moving = true;
				}
				if ( k.key_status_down( Game_Input.Index.a ) )
				{
					nv += new Vector3(0,0,v);
					moving = true;
				}
				if ( moving )
				{
					nv.Normalize();
					nv *= v;
				}
				rigidbody.velocity = nv;
			} else
			if ( update_method == Update_Method.UNITY )
			{
				Vector3 nv = Vector3.zero;
				bool moving = false;
				if ( Keyboard.current.wKey.isPressed )
				{
					nv += new Vector3(v,0,0);
					moving = true;
				}
				if ( Keyboard.current.sKey.isPressed )
				{
					nv += new Vector3(-v,0,0);
					moving = true;
				}
				if ( Keyboard.current.dKey.isPressed )
				{
					nv += new Vector3(0,0,-v);
					moving = true;
				}
				if ( Keyboard.current.aKey.isPressed )
				{
					nv += new Vector3(0,0,v);
					moving = true;
				}
				if ( moving )
				{
					nv.Normalize();
					nv *= v;
				}
				rigidbody.velocity = nv;
			}
		}
		private void Update()
		{
			if ( Keyboard.current.digit1Key.wasPressedThisFrame )
			{
				update_method = Update_Method.UNITY;
				update_method_text.text = update_method.ToString();
			} else
			if ( Keyboard.current.digit2Key.wasPressedThisFrame )
			{
				update_method = Update_Method.RAW;
				update_method_text.text = update_method.ToString();
			}
		}

		private void OnCollisionEnter( Collision collision )
		{
			string contacts = $"{collision.contactCount} contacts\n";
			for ( int i = 0; i != collision.contactCount; ++i )
			{
				ContactPoint c = collision.GetContact( i );
				contacts += $"{i,2}) normal: {c.normal}\n    point: {c.point}\n    sep: {c.separation}\n    this: {c.thisCollider.name}\n    other: {c.otherCollider.name}";

				if ( spawn_debug_arrows )
				{
					GameObject go = Instantiate( collision_marker_prefab, null, false );
					Quaternion r = Quaternion.FromToRotation( Vector3.up, c.normal );
					go.transform.SetPositionAndRotation( c.point, r );
				}
			}
			log(
			   $"{collision.gameObject.name}\n" +
			   $"{collision.relativeVelocity}\n" +
			   $"{collision.rigidbody}\n" +
			   $"{collision.rigidbody?.name}\n" +
			   $"{contacts}"
			);
		}
		#endregion
	}
}
