using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Text;

using static tg.tg;

namespace tg
{
	public delegate IntPtr WndProcDelegate(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);
	public class WindowHandleGrabber : MonoBehaviour
	{
		//private const string UnityWindowClassName = "UnityWndClass";
		#if UNITY_EDITOR
		private const string UnityWindowClassName = "UnityContaine";
		#else
		private const string UnityWindowClassName = "Uni";
		#endif

		[DllImport( "kernel32.dll" )]
		static extern uint GetCurrentThreadId();

		[DllImport( "user32.dll", CharSet = CharSet.Auto, SetLastError = true )]
		static extern int GetClassName( IntPtr hWnd, StringBuilder lpString, int nMaxCount );

		public delegate bool EnumWindowsProc( IntPtr hWnd, IntPtr lParam );
		[DllImport( "user32.dll" )]
		[return: MarshalAs( UnmanagedType.Bool )]
		static extern bool EnumThreadWindows( uint dwThreadId, EnumWindowsProc lpEnumFunc, IntPtr lParam );

		[DllImport( "user32.dll", CharSet = CharSet.Auto, ExactSpelling = true )]
		public static extern IntPtr GetForegroundWindow();

		[DllImport( "user32.dll" )]
		private static extern IntPtr GetActiveWindow();

		[DllImport( "user32.dll", SetLastError = true )]
		static extern IntPtr SetWindowLong( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		// NOTE(theGiallo): https://social.msdn.microsoft.com/Forums/vstudio/en-US/b6c1cacc-3c84-4f84-82d0-d3f7558c1c0d/how-to-use-quotsetwindowlongquot-using-c?forum=csharpgeneral

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "SetWindowLongW" )]
		private static extern IntPtr SetWindowLongPtr32( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "SetWindowLongPtrW" )]
		private static extern IntPtr SetWindowLongPtr64( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "GetWindowLongW" )]
		private static extern IntPtr GetWindowLongPtr32( IntPtr hWnd, int nIndex );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "GetWindowLongPtrW" )]
		private static extern IntPtr GetWindowLongPtr64( IntPtr hWnd, int nIndex );




		[DllImport( "kernel32.dll", SetLastError = true )]
		static extern void SetLastError( int error );

		[DllImport( "user32.dll" )]
		static extern IntPtr CallWindowProc( IntPtr lpPrevWndFunc, IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam );


		public struct Window_Event
		{
			public IntPtr hWnd;
			public uint   msg;
			public IntPtr wParam;
			public IntPtr lParam;
			public bool[] processed;
		}
		public static IntPtr window_handle { get; private set; }
		public static EventHandler<Window_Event> window_event_handler;

		public  IntPtr interactionWindow;
		private IntPtr oldWndProcPtr;
		private IntPtr newWndProcPtr;
		private WndProcDelegate newWndProc;
		private bool is_running = false;



		private void Awake()
		{
			if ( is_running ) return;

			DontDestroyOnLoad( gameObject );

			window_handle = GetForegroundWindow();
			log( $"[GetForegroundWindow()] window handle = {window_handle} 0x{window_handle.ToString( "X" )}" );

			window_handle = GetActiveWindow();
			log( $"[GetActiveWindow()] window handle = {window_handle} 0x{window_handle.ToString( "X" )}" );

			uint threadId = GetCurrentThreadId();
			EnumThreadWindows( threadId,
			( hWnd, lParam ) =>
			{
				var classText = new StringBuilder( UnityWindowClassName.Length + 1 );
				GetClassName( hWnd, classText, classText.Capacity );

				log( $"{classText.ToString()} {hWnd} 0x{hWnd.ToString( "X" )}" );

				if ( classText.ToString() == UnityWindowClassName )
				{
					window_handle = hWnd;
					//return false;
				}
				return true;
			}, IntPtr.Zero );

			log( $"[EnumThreadWindows] window handle = {window_handle} 0x{window_handle.ToString( "X" )}" );

			newWndProc    = new WndProcDelegate( wndProc );
			newWndProcPtr = Marshal.GetFunctionPointerForDelegate( newWndProc );

			SetLastError( 0 );

			#if true
			if ( IntPtr.Size == 4 )
			{
				oldWndProcPtr = SetWindowLongPtr32( window_handle, GWL_WNDPROC, newWndProcPtr );
			} else
			{
				oldWndProcPtr = SetWindowLongPtr64( window_handle, GWL_WNDPROC, newWndProcPtr );
			}
			#else
			oldWndProcPtr = SetWindowLong( window_handle, GWL_WNDPROC, newWndProcPtr );
			#endif

			if ( (int)oldWndProcPtr == 0 )
			{
				int err = Marshal.GetLastWin32Error();
				log_err( $"failed to register window procedure {err}" );
			}
			is_running     = true;
		}

		private void OnApplicationFocus( bool focus )
		{
			if ( ! focus )
			{
				log_warn( "lost focus" );
				return;
			}
			log_warn( "got focus" );

			IntPtr curr_WndProcPtr;

			SetLastError( 0 );
			if ( IntPtr.Size == 4 )
			{
				curr_WndProcPtr = GetWindowLongPtr32( window_handle, GWL_WNDPROC );
			} else
			{
				curr_WndProcPtr = GetWindowLongPtr64( window_handle, GWL_WNDPROC );
			}
			if ( (int)oldWndProcPtr == 0 )
			{
				int err = Marshal.GetLastWin32Error();
				log_err( $"failed to register window procedure {err}" );
			}

			if ( curr_WndProcPtr != newWndProcPtr )
			{
				SetLastError( 0 );
				if ( IntPtr.Size == 4 )
				{
					oldWndProcPtr = SetWindowLongPtr32( window_handle, GWL_WNDPROC, newWndProcPtr );
				} else
				{
					oldWndProcPtr = SetWindowLongPtr64( window_handle, GWL_WNDPROC, newWndProcPtr );
				}
				if ( (int)oldWndProcPtr == 0 )
				{
					int err = Marshal.GetLastWin32Error();
					log_err( $"failed to register window procedure {err}" );
				}
			} else
			{
				IntPtr new_oldWndProcPtr;
				SetLastError( 0 );
				if ( IntPtr.Size == 4 )
				{
					new_oldWndProcPtr = SetWindowLongPtr32( window_handle, GWL_WNDPROC, newWndProcPtr );
				} else
				{
					new_oldWndProcPtr = SetWindowLongPtr64( window_handle, GWL_WNDPROC, newWndProcPtr );
				}
				if ( new_oldWndProcPtr != newWndProcPtr )
				{
					log( "ptr changed" );
				}
				if ( (int)new_oldWndProcPtr == 0 )
				{
					int err = Marshal.GetLastWin32Error();
					log_err( $"failed to register window procedure {err}" );
				}
			}
		}

		private static
		IntPtr
		StructToPtr( object obj )
		{
			var ptr = Marshal.AllocHGlobal( Marshal.SizeOf( obj ) );
			Marshal.StructureToPtr( obj, ptr, false );
			return ptr;
		}

		// NOTE(theGiallo): https://docs.microsoft.com/en-us/windows/win32/winmsg/wm-user
		public static readonly uint WM_USER = 0x0400;
		public static readonly uint WM_MINE = WM_USER + 100;
		public static readonly uint WM_MOUSEMOVE = 0x0200;
		// NOTE(theGiallo): https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-input
		public static readonly uint WM_INPUT = 0x00FF;

		// NOTE(theGiallo): https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setwindowlonga
		// Sets a new address for the window procedure.
		// You cannot change this attribute if the window does not belong to the same process as the calling thread.
		static readonly int  GWL_WNDPROC = -4;

		private
		IntPtr
		wndProc( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
		{
			IntPtr ret;

			if ( this == null || ! is_running || oldWndProcPtr == IntPtr.Zero || window_handle == IntPtr.Zero )
			{
				return (IntPtr)1;
			}

			//log( $"{msg}" );

			//if ( msg == WM_MOUSEMOVE )
			//{
			//	log( "WM_MOUSEMOVE" );
			//}
			//if ( msg == WM_MINE )
			{
				//event_handler( hWnd, msg, wParam, lParam );
				//if ( msg == 255 )
				//{
				//	return (IntPtr)0;
				//}
			}
			if ( event_handler( hWnd, msg, wParam, lParam ) )
			{
				//log( "did process msg" );
				ret = (IntPtr)0;
			} else
			{
				//log( "did not process msg" );
				ret = CallWindowProc( oldWndProcPtr, hWnd, msg, wParam, lParam );
			}
			//log( $"old proc returned {(int)ret}" );
			//if ( msg == 255 )
			//{
			//	ret = (IntPtr)1;
			//}
			return ret;
		}
		private
		bool
		event_handler( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
		{
			//log( $"event_handler( hWnd = {hWnd}, msg = {msg}, wParam = {wParam}, lParam = {lParam} )" );
			bool[] processed = new bool[]{ false };
			window_event_handler?.Invoke( this, new Window_Event(){ hWnd = hWnd, msg = msg, wParam = wParam, lParam = lParam, processed = processed } );
			return processed[0];
		}

		void OnDisable()
		{
			if ( !is_running ) return;

			Debug.Log( "Uninstall Hook" );
			#if true
			if ( IntPtr.Size == 4 )
			{
				oldWndProcPtr = SetWindowLongPtr32( window_handle, GWL_WNDPROC, oldWndProcPtr );
			} else
			{
				oldWndProcPtr = SetWindowLongPtr64( window_handle, GWL_WNDPROC, oldWndProcPtr );
			}
			#else
			SetWindowLong( window_handle, GWL_WNDPROC, oldWndProcPtr );
			#endif

			window_handle = IntPtr.Zero;
			oldWndProcPtr = IntPtr.Zero;
			newWndProcPtr = IntPtr.Zero;
			newWndProc    = null;
			is_running     = false;
		}
	}
}