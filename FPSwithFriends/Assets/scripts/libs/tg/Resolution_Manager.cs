﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Resolution_Manager : MonoBehaviour
	{
		public struct
		Res_Change
		{
			public Vector2Int old_res;
			public Vector2Int new_res;
		}
		public class
		Render_Camera_Target
		{
			public Camera        camera;
			public RenderTexture rt;
			public RawImage      raw_image;
			// NOTE(theGiallo): resolution scale respect to the current main rt resolution
			public Vector2       res_scale;
		}

		//public static RawImage main_camera_raw_image;
		public static EventHandler<Res_Change> on_res_change;

		public static void
		register( Render_Camera_Target rct )
		{
			rcts.Add( rct );
			update_rct( rct );
		}
		private static void update_rct( Render_Camera_Target rct )
		{
			int scaled_w = (int)( current_used_width  * rct.res_scale.x );
			int scaled_h = (int)( current_used_height * rct.res_scale.y );
			if ( rct.rt == null )
			{
				rct.rt = new RenderTexture( scaled_w,
				                            scaled_h,
				                            24,
				                            RenderTextureFormat.ARGB32 );
			} else
			{
				rct.rt = new RenderTexture( scaled_w,
				                            scaled_h,
				                            rct.rt.depth,
				                            rct.rt.format );
			}

			if ( rct.raw_image != null )
			{
				rct.raw_image.texture = rct.rt;
			}
			rct.camera.targetTexture = rct.rt;
		}

		public static void
		set_max_resolution( Vector2Int max_res )
		{
			current_max_res = max_res;
			int w = Screen.width;
			int h = Screen.height;
			Vector2Int old_res = new Vector2Int( current_used_width, current_used_height );
			int used_w = current_used_width;
			int used_h = current_used_height;
			bool changed = false;
			if ( w >= h && max_res.x >= max_res.y )
			{
				used_w = Math.Min( w, max_res.x );
				used_h = (int)( h * used_w / (float)w );
			} else
			if ( w < h && max_res.x < max_res.y )
			{
				used_h = Math.Min( h, max_res.y );
				used_w = (int)( w * used_h / (float)h );
			} else
			if ( w <= h && max_res.x >= max_res.y )
			{
				used_h = Math.Min( h, max_res.x );
				used_w = (int)( w * used_h / (float)h );
			} else
			if ( w >= h && max_res.x <= max_res.y )
			{
				used_w = Math.Min( w, max_res.y );
				used_h = (int)( h * used_w / (float)w );
			}
			changed = current_used_width  != used_w
			       || current_used_height != used_h;
			current_used_width  = used_w;
			current_used_height = used_h;
			if ( changed )
			{
				for ( int i = 0; i != rcts.Count; ++i )
				{
					update_rct( rcts[i] );
				}
				Res_Change res_change = new Res_Change(){ old_res = old_res, new_res = new Vector2Int( used_w, used_h ) };
				log( $"changed resolution {res_change.old_res} => {res_change.new_res}" );
				if ( Screen.fullScreenMode == FullScreenMode.ExclusiveFullScreen )
				{
					Screen.SetResolution( used_w, used_h, Screen.fullScreenMode );
				}
				on_res_change?.Invoke( instance, res_change );
			}
		}

		#if false
		public static void
		sed_depth( int new_depth )
		{
			if ( new_depth != main_rt_depth )
			{
				main_rt.depth = new_depth;
				main_rt_depth = main_rt.depth;
			}
		}

		public static void
		set_texture_format( RenderTextureFormat new_rt_format )
		{
			if ( main_rt_format != new_rt_format )
			{
				main_rt.format = new_rt_format;
				main_rt_format = main_rt.format;
			}
		}
		private static RenderTexture main_rt;
		private static int main_rt_depth;
		private static RenderTextureFormat main_rt_format;
		#endif

		#region private
		private static int current_used_width;
		private static int current_used_height;
		private static Vector2Int current_max_res;

		private int last_screen_width;
		private int last_screen_height;

		private static List<Render_Camera_Target> rcts = new List<Render_Camera_Target>();

		#region static
		private static Resolution_Manager instance;
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		private static
		void
		ensure_instanced()
		{
			if ( instance != null )
			{
				return;
			}
			GameObject go = new GameObject( "Resolution Manager" );
			instance = go.AddComponent<Resolution_Manager>();
		}
		#endregion

		#endregion

		#region Unity
		private void Awake()
		{
			DontDestroyOnLoad( gameObject );
			current_used_width  = last_screen_width  = Screen.width;
			current_used_height = last_screen_height = Screen.height;
			//main_rt = new RenderTexture( current_used_width, current_used_height, main_rt_depth, main_rt_format );
			set_max_resolution( new Vector2Int( Screen.width, Screen.height ) );
		}
		private void Start()
		{
		}
		private void Update()
		{
			if ( last_screen_width != Screen.width
			  || last_screen_height != Screen.height )
			{
				set_max_resolution( current_max_res );
			}
		}
		#endregion
	}
}
