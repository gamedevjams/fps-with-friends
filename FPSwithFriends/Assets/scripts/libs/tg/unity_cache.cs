﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class unity_cache : MonoBehaviour
	{
		private static unity_cache instance;

		public static class
		Application
		{
			public static string persistentDataPath;
		}

		#region private
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		private
		static
		void
		ensure_instance()
		{
			if ( instance == null )
			{
				GameObject go = new GameObject( "unity_cache" );
				instance = go.AddComponent<unity_cache>();
			}
		}

		private
		void
		gather()
		{
			Application.persistentDataPath = UnityEngine.Application.persistentDataPath;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			DontDestroyOnLoad( gameObject );
			gather();
		}
		private void Start()
		{
		}
		private void Update()
		{
			gather();
		}
		#endregion
	}
}
