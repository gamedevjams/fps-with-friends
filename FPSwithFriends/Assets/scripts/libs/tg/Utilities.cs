﻿#if !BIT_VECTOR_BYTE_BLOCKS && !BIT_VECTOR_WORD_BLOCKS
#define BIT_VECTOR_WORD_BLOCKS
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace tg
{
	public static partial class
	tg
	{
		public
		static
		unsafe
		uint
		u32_FNV1a( byte*  bytes, int length )
		{
			uint ret;

			const uint FNV_prime    = 16777619;
			const uint offset_basis = 2166136261;

			ret = offset_basis;
			for ( uint i = 0; i < length; ++i )
			{
				ret = ret ^ (uint)bytes[i];
				ret = ret * FNV_prime;
			}

			return ret;
		}
		public
		static
		uint
		u32_FNV1a( byte[] bytes )
		{
			uint ret;

			const uint FNV_prime    = 16777619;
			const uint offset_basis = 2166136261;

			ret = offset_basis;
			for ( uint i = 0; i < bytes.Length; ++i )
			{
				ret = ret ^ (uint)bytes[i];
				ret = ret * FNV_prime;
			}

			return ret;
		}
		public
		static
		uint
		u32_FNV1a( string s )
		{
			uint ret;

			byte[] bytes = System.Text.Encoding.UTF8.GetBytes( s );

			ret = u32_FNV1a( bytes );

			return ret;
		}
		public
		static
		int
		s32_FNV1a( byte[] bytes )
		{
			int ret;
			uint u = u32_FNV1a( bytes );
			ret = System.BitConverter.ToInt32( System.BitConverter.GetBytes( u ), 0 );
			return ret;
		}
		public
		static
		int
		s32_FNV1a( string s )
		{
			int ret;
			uint u = u32_FNV1a( s );
			ret = System.BitConverter.ToInt32( System.BitConverter.GetBytes( u ), 0 );
			return ret;
		}

		public
		static
		float
		deg_to_0_360( float deg )
		{
			float ret = deg - Mathf.Floor( deg / 360 ) * 360;
			ret = ret < 0 ? ret + 360 : ret;
			return ret;
		}

		public static float clamp_deg_0_360( float deg_0360, float min_deg_0360, float max_deg_0360 )
		{
			float ret;
			if ( min_deg_0360 > max_deg_0360 )
			{
				ret = deg_0360 < max_deg_0360
				    ? deg_0360
				    : ( deg_0360 > min_deg_0360
				      ? deg_0360
				      : ( deg_0360 <= 180
				        ? max_deg_0360
				        : min_deg_0360 ) );
			} else
			{
				ret = Mathf.Clamp( deg_0360, min_deg_0360, max_deg_0360 );
			}
			return ret;
		}

		public
		static
		float
		deg_to_center0( float deg )
		{
			float ret = deg_to_0_360( deg + 180 ) - 180;
			return ret;
		}

		public
		static
		float
		clamp_deg_center0( float deg_center0, float min_deg_center0, float max_deg_center0 )
		{
			float ret = Mathf.Clamp( deg_center0, min_deg_center0, max_deg_center0 );
			return ret;
		}

		public
		static
		float
		clamp_deg_to_center0( float deg )
		{
			float ret = Mathf.Clamp( deg, -180, 180 );
			return ret;
		}

		public
		static
		bool
		f_eps( float f0, float f1, float eps )
		{
			bool ret = Mathf.Abs( f0 - f1 ) < eps;
			return ret;
		}

		public static
		bool
		v3_eps( Vector3 v0, Vector3 v1, float eps )
		{
			bool ret = f_eps( v0.x, v0.x, eps ) && f_eps( v0.y, v1.y, eps ) && f_eps( v0.z, v1.z, eps );
			return ret;
		}

		public
		static
		bool
		same_sign( float a, float b )
		{
			bool ret = ( a > 0 && b > 0 ) || ( a < 0 && b < 0 ) || ( a == 0 && b == 0 );
			return ret;
		}
		public
		static
		bool
		a_greater_in_module_than_b_and_same_sign( float a, float b )
		{
			bool ret = same_sign( a, b ) && Mathf.Abs( a ) > Mathf.Abs( b );
			return ret;
		}

		public
		static
		float
		get_angle_deg_to_align_two_points_to_target( Vector2 target, Vector2 forward_point, Vector2 backward_point )
		{
			Vector2 T = target;
			Vector2 P = forward_point;
			Vector2 B = backward_point;

			Vector2 BOdir2   = -B.normalized;
			Vector2 BPdir2   = ( P - B ).normalized;
			//Vector3 BOdir3   = new Vector3( BOdir2.x, 0, BOdir2.y );
			//Vector3 BPdir3   = new Vector3( BPdir2.x, 0, BPdir2.y );
			//float sin_beta   = Vector3.Cross( BOdir3, BPdir3 ).y;
			float deg_beta   = Vector2.Angle( BOdir2, BPdir2 );
			float sin_beta   = Mathf.Sin( Mathf.Deg2Rad * deg_beta );
			//float sin_alpha  = Vector3.Cross( P.normalized, B.normalized ).magnitude;
			float sin_delta  = sin_beta * B.magnitude / T.magnitude;
			float deg_delta  = Mathf.Rad2Deg * Mathf.Asin( sin_delta );
			float deg_alpha  = 180 - deg_beta - deg_delta;
			float deg_alpha_ = Vector2.Angle( T, B );

			float y_rotation  = deg_alpha - deg_alpha_;
			return y_rotation;
		}

		public
		class
		Timer
		{
			public  bool  active = false;
			private float target_time;
			private float duration;
			public  bool use_fixed_time = false;

			public Timer( float duration, bool autostart = false, float delay = 0 )
			{
				this.duration = duration + delay;
				if ( autostart )
				{
					restart();
				}
				this.duration = duration;
			}
			public
			void
			restart( float duration )
			{
				this.duration = duration;
				target_time = time() + duration;
				active = true;
			}
			public
			void
			restart()
			{
				target_time = time() + duration;
				active = true;
			}
			public
			float
			remaining_time()
			{
				float ret = target_time - time();
				return ret;
			}
			public
			float
			passed_time()
			{
				float ret = time() - target_time + duration;
				return ret;
			}
			public
			float
			remaining_percentage()
			{
				float ret = remaining_time() / duration;
				return ret;
			}
			public
			float
			passed_percentage()
			{
				float ret = passed_time() / duration;
				return ret;
			}
			public
			bool
			loopback()
			{
				bool ret = false;
				float t = remaining_time();
				if ( t <= 0 )
				{
					ret = true;
					target_time = time() + duration + t;
				}
				return ret;
			}

			private
			float
			time()
			{
				float ret = use_fixed_time ? Time.fixedTime : Time.time;
				return ret;
			}
		}

		public
		struct
		Bit_Vector
		{
			#if BIT_VECTOR_BYTE_BLOCKS
			private byte[] array;
			#elif BIT_VECTOR_WORD_BLOCKS
			private uint[]  array;
			#endif
			public int bits_count { get; private set; }
			public int bits_real_capacity { get; private set; }

			public
			Bit_Vector( int bits_count, bool round_up_count = false )
			{
				#if BIT_VECTOR_BYTE_BLOCKS
				array = new byte[ ( bits_count + 7 ) / 8 ];
				#elif BIT_VECTOR_WORD_BLOCKS
				array = new uint[ ( bits_count + 31 ) / 32 ];
				#endif
				this.bits_real_capacity = real_capacity_of_array( array.Length );
				this.bits_count = round_up_count ? this.bits_real_capacity : bits_count;
			}
			public
			static
			int real_capacity_of_array( int array_length )
			{
				int ret;

				#if BIT_VECTOR_BYTE_BLOCKS
				ret = 8 * array_length;
				#elif BIT_VECTOR_WORD_BLOCKS
				ret = 32 * array_length;
				#endif

				return ret;
			}
			public
			int
			get_blocks_used_count()
			{
				int ret;
				int tmp;
				get_block_bit( bits_count - 1, out ret, out tmp );
				++ret;
				return ret;
			}
			public
			int this[int index]
			{
				get
				{
					int ret;
					int block_index, block_bit_index;
					get_block_bit( index, out block_index, out block_bit_index );
					ret = (int)( ( array[block_index] >> block_bit_index ) & 1u );
					return ret;
				}
				set
				{
					int block_index, block_bit_index;
					get_block_bit( index, out block_index, out block_bit_index );
					debug_assert( array != null );
					array[block_index] = value == 0 ? array[block_index] & ~( 1u << block_bit_index )
					                                : array[block_index] |  ( 1u << block_bit_index );
				}
			}
			public
			void
			set( int index, int value )
			{
				int block_index, block_bit_index;
				get_block_bit( index, out block_index, out block_bit_index );
				array[block_index] = value == 0 ? array[block_index] & ~( 1u << block_bit_index )
				                                : array[block_index] |  ( 1u << block_bit_index );
			}

			public
			void
			clear()
			{
				for ( int i = 0; i != array.Length; ++i )
				{
					array[i] = 0;
				}
			}

			public
			void
			all_to_1()
			{
				for ( int i = 0; i != array.Length; ++i )
				{
					array[i] =
					#if BIT_VECTOR_BYTE_BLOCKS
					0xff;
					#elif BIT_VECTOR_WORD_BLOCKS
					0xffffffff;
					#endif
				}
			}

			public
			void
			negate()
			{
				for ( int i = 0; i != array.Length; ++i )
				{
					array[i] = ~array[i];
				}
			}

			public
			void
			copy_from( ref Bit_Vector other )
			{
				int other_blocks_count = other.get_blocks_used_count();
				if ( array == null || array.Length < other_blocks_count )
				{
					array = new
					#if BIT_VECTOR_BYTE_BLOCKS
					byte
					#elif BIT_VECTOR_WORD_BLOCKS
					uint
					#endif
					[other_blocks_count];

					bits_real_capacity = real_capacity_of_array( array.Length );
				}
				for ( int i = 0; i != other_blocks_count; ++i )
				{
					array[i] = other.array[i];
				}
				bits_count = other.bits_count;
			}

			public
			Bit_Vector
			copy_whole( uint u )
			{
				#if BIT_VECTOR_BYTE_BLOCKS
				private byte[] array;
				array[0] = ( u >> 24 ) & 0xff;
				array[1] = ( u >> 16 ) & 0xff;
				array[2] = ( u >>  8 ) & 0xff;
				array[3] = ( u       ) & 0xff;
				#elif BIT_VECTOR_WORD_BLOCKS
				array[0] = u;
				#endif
				return this;
			}

			private
			void
			get_block_bit( int bit_index, out int block_index, out int block_bit_index )
			{
				#if BIT_VECTOR_BYTE_BLOCKS
				block_index = bit_index / 8;
				block_bit_index = 7 - ( bit_index - block_index * 8 );
				#elif BIT_VECTOR_WORD_BLOCKS
				block_index = bit_index / 32;
				block_bit_index = 31 - ( bit_index - block_index * 32 );
				#endif
			}

			public
			Bit_Vector
			clone()
			{
				Bit_Vector ret = new Bit_Vector( bits_count );
				ret.copy_from( ref this );
				return ret;
			}

			public static
			Bit_Vector
			operator &( Bit_Vector v0, Bit_Vector v1 )
			{
				debug_assert( v0.bits_real_capacity == v1.bits_real_capacity );
				Bit_Vector ret = v0.clone();

				for ( int i = 0; i != ret.array.Length; ++i )
				{
					ret.array[i] = v0.array[i] & v1.array[i];
				}

				return ret;
			}
			public static
			Bit_Vector
			operator |( Bit_Vector v0, Bit_Vector v1 )
			{
				debug_assert( v0.bits_real_capacity == v1.bits_real_capacity );
				Bit_Vector ret = v0.clone();

				for ( int i = 0; i != ret.array.Length; ++i )
				{
					ret.array[i] = v0.array[i] | v1.array[i];
				}

				return ret;
			}
			public static
			Bit_Vector
			operator ~( Bit_Vector v0 )
			{
				Bit_Vector ret = v0.clone();
				for ( int i = 0; i != v0.array.Length; ++i )
				{
					ret.array[i] = ~v0.array[i];
				}

				return ret;
			}

			public override
			string
			ToString()
			{
				string ret;
				char[] chars = new char[bits_count];
				for ( int i = 0; i != bits_count; ++i )
				{
					chars[i] = (char) ( '0' + this[i] );
				}
				ret = new string( chars );
				return ret;
			}
		}

		public
		struct
		Flat_Hyper_Matrix<T>
		{
			public  int   dimensions_count { get { return _sizes.Length; } }
			public int[] _sizes;
			public
			int[]
			get_sizes_copy()
			{
				int[] ret = new int[_sizes.Length];
				_sizes.CopyTo( ret, 0 );
				return ret;
			}
			private int[] sizes_multiplied;
			public  T[]   array;

			public
			Flat_Hyper_Matrix( params int[] sizes )
			{
				this._sizes            = new int[sizes.Length];
				this.sizes_multiplied = new int[sizes.Length];
				array = new T[mult( sizes )];
				sizes_multiplied[0] = 1;
				this._sizes[0] = sizes[0];
				for ( int i = 1; i != sizes.Length; ++i )
				{
					this._sizes[i] = sizes[i];
					sizes_multiplied[i] = sizes[i - 1] * sizes_multiplied[i - 1];
				}
			}
			public
			ref T this[params int[] indexes]
			{
				get
				{
					ref T ret = ref array[index(indexes)];
					return ref ret;
				}
			}
			public
			ref T at( params int[] point )
			{
				ref T ret = ref array[ index( point ) ];
				return ref ret;
			}
			public
			int
			index( params int[] point )
			{
				int ret = 0;
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					ret += point[i] * sizes_multiplied[i];
				}
				return ret;
			}
			public
			int[]
			get_point_from_index( int index )
			{
				int[] ret = new int[dimensions_count];
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					int m = i != dimensions_count - 1 ? sizes_multiplied[ i + 1 ] : array.Length;
					ret[i] = ( index % m ) / sizes_multiplied[i];
				}
				return ret;
			}
		}

		public
		struct
		Flat_Bit_Hyper_Matrix
		{
			public  int   dimensions_count { get { return _sizes.Length; } }
			public int[] _sizes;
			public
			int[]
			get_sizes_copy()
			{
				int[] ret = new int[_sizes.Length];
				_sizes.CopyTo( ret, 0 );
				return ret;
			}
			private int[] sizes_multiplied;
			public Bit_Vector bit_vector;

			public
			Flat_Bit_Hyper_Matrix( params int[] sizes )
			{
				this._sizes            = new int[sizes.Length];
				this.sizes_multiplied = new int[sizes.Length];
				bit_vector = new Bit_Vector( mult( sizes ) );
				sizes_multiplied[0] = 1;
				for ( int i = 1; i != sizes.Length; ++i )
				{
					this._sizes[i] = sizes[i];
					sizes_multiplied[i] = sizes[i - 1] * sizes_multiplied[i - 1];
				}
			}
			public
			int this[params int[] indexes]
			{
				get
				{
					int ret = bit_vector[index(indexes)];
					return ret;
				}
				set
				{
					bit_vector[index(indexes)] = value;
				}
			}
			public
			void
			set_b( bool b, params int[] point )
			{
				bit_vector[ index( point ) ] = b ? 1 : 0;
			}
			public
			int
			at( params int[] point )
			{
				int ret = bit_vector[ index( point ) ];
				return ret;
			}
			public
			int
			index( params int[] point )
			{
				int ret = 0;
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					ret += point[i] * sizes_multiplied[i];
				}
				return ret;
			}
			public
			int[]
			get_point_from_index( int index )
			{
				int[] ret = new int[dimensions_count];
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					int m = i != dimensions_count - 1 ? sizes_multiplied[ i + 1 ] : bit_vector.bits_count;
					ret[i] = ( index % m ) / sizes_multiplied[i];
				}
				return ret;
			}
		}


		public
		struct
		Multi_Dictionary<K,V>
		{
			public Dictionary<K,List<V>> dictionary;
			public
			void
			init()
			{
				dictionary = new Dictionary<K, List<V>>();
			}
			public
			List<V> this[K key]
			{
				get
				{
					TryGetValues( key, out List<V> ret );
					return ret;
				}
				set
				{
					dictionary[key] = value;
				}
			}
			public
			void
			Add( K key, V value )
			{
				List<V> lv;
				if ( !dictionary.TryGetValue( key, out lv ) )
				{
					lv = new List<V>();
					dictionary[key] = lv;
				}
				lv.Add( value );
			}
			public
			bool
			TryGetValues( K key, out List<V> values )
			{
				bool ret = dictionary.TryGetValue( key, out values );
				return ret;
			}
			public
			bool
			TryGetFirst( K key, ref V value )
			{
				bool ret;
				List<V> l;
				ret = dictionary.TryGetValue( key, out l );
				if ( ret )
				{
					value = l[0];
				}
				return ret;
			}
		}

		public
		struct
		Stack<T>
		{
			Chunked_Array<T> array;
			public
			int
			size()
			{
				int ret = array.occupancy;
				return ret;
			}

			public
			void
			init()
			{
				array = new Chunked_Array<T>( -1 );
			}
			public
			void
			push( ref T e )
			{
				array.add( ref e );
			}
			public
			void
			push( T e )
			{
				array.add( e );
			}
			public
			bool
			pop( ref T e )
			{
				bool ret = array.occupancy != 0;
				if ( ret )
				{
					e = ref array.last();
					array.remove_last();
				}
				return ret;
			}
			public
			bool
			peek( ref T e )
			{
				bool ret = array.occupancy != 0;
				if ( ret )
				{
					e = ref array.last();
				}
				return ret;
			}
			public
			ref T
			peek()
			{
				ref T ret = ref array.last();
				return ref ret;
			}
			public
			ref T
			Peek()
			{
				if ( array.occupancy == 0 )
				{
					throw new System.InvalidOperationException();
				}
				ref T ret = ref array.last();
				return ref ret;
			}
		}
		[Serializable]
		public
		class
		Chunked_Array<T> : ISerializationCallbackReceiver
		{
			public List<T[]> chunks;
			//public List<int> chunks_occupancy;
			public int first_free_chunk { get; private set; }
			public int first_free_chunk_occupancy { get; private set; }
			public int occupancy { get; private set; }
			public int chunks_size { get => _chunks_size; set { _chunks_size = value; } }
			[SerializeField]
			private int _chunks_size;
			public
			Chunked_Array( int chunks_size = 1024, uint initial_chunks_count = 0 )
			{
				init( chunks_size, initial_chunks_count );
			}
			private
			void
			init( int chunks_size = 1024, uint initial_chunks_count = 0 )
			{
				this.chunks_size = chunks_size <= 0 ? 1024 : chunks_size;
				chunks = new List<T[]>();
				//chunks_occupancy = new List<int>();
				first_free_chunk = -1;
				first_free_chunk_occupancy = 0;
				occupancy = 0;
				for ( uint i = 0; i != initial_chunks_count; ++i )
				{
					add_chunk();
				}
			}
			public
			void
			add_chunk()
			{
				chunks.Add( new T[ chunks_size ] );
				//chunks_occupancy.Add( 0 );
				first_free_chunk = first_free_chunk < 0 ? chunks.Count - 1 : first_free_chunk;
			}
			public
			void
			add( ref T e )
			{
				if ( first_free_chunk < 0 )
				{
					add_chunk();
				}
				chunks[first_free_chunk][first_free_chunk_occupancy++] = e;
				++occupancy;
				if ( first_free_chunk_occupancy == chunks_size )
				{
					first_free_chunk =
					   first_free_chunk == chunks.Count - 1 ? -1 : first_free_chunk + 1;
					first_free_chunk_occupancy = 0;
				}
			}
			public
			void
			add( T e )
			{
				if ( first_free_chunk < 0 )
				{
					add_chunk();
				}
				chunks[first_free_chunk][first_free_chunk_occupancy++] = e;
				++occupancy;
				if ( first_free_chunk_occupancy == chunks_size )
				{
					first_free_chunk =
					   first_free_chunk == chunks.Count - 1 ? -1 : first_free_chunk + 1;
					first_free_chunk_occupancy = 0;
				}
			}
			public
			void
			add( T[] ca )
			{
				for ( int i = 0; i != ca.Length; ++i )
				{
					add( ca[i] );
				}
			}
			public
			void
			add( List<T> ca )
			{
				for ( int i = 0; i != ca.Count; ++i )
				{
					add( ca[i] );
				}
			}
			public
			void
			add( Chunked_Array<T> ca )
			{
				for ( int i = 0; i != ca.occupancy; ++i )
				{
					add( ca[i] );
				}
			}
			public
			bool
			remove_last()
			{
				bool ret = false;

				if ( occupancy != 0 )
				{
					if ( first_free_chunk == -1 )
					{
						first_free_chunk = chunks.Count - 1;
						first_free_chunk_occupancy = chunks_size - 1;
					} else
					if ( first_free_chunk_occupancy == 0 )
					{
						--first_free_chunk;
						first_free_chunk_occupancy = chunks_size - 1;
					} else
					{
						--first_free_chunk_occupancy;
					}
					--occupancy;
					ret = true;
				}

				return ret;
			}
			public
			ref T this[ int index ]
			{
				get
				{
					int ci = index / chunks_size;
					int i  = index - chunks_size * ci;
					debug_assert( ci <= first_free_chunk && ( ci != first_free_chunk || i < first_free_chunk_occupancy ) );
					ref T ret = ref chunks[ci][i];
					return ref ret;
				}
			}
			public
			ref T last()
			{
				int index = occupancy - 1;
				int ci = index / chunks_size;
				int i  = index - chunks_size * ci;
				ref T ret = ref chunks[ci][i];
				return ref ret;
			}

			public
			void
			clear()
			{
				first_free_chunk = 0;
				first_free_chunk_occupancy = 0;
				occupancy = 0;
			}

			public
			void
			copy_from( Chunked_Array<T> other )
			{
				clear();
				for ( int i = 0; i != other.occupancy; ++i )
				{
					add( ref other[i] );
				}
			}

			[SerializeField]
			private T[] __arr_for_unity;
			public void OnBeforeSerialize()
			{
				__arr_for_unity = new T[occupancy];
				for ( int i = 0; i != occupancy; ++i )
				{
					__arr_for_unity[i] = this[i];
				}
			}

			public void OnAfterDeserialize()
			{
				init( chunks_size, 1 );
				for ( int i = 0; i != __arr_for_unity.Length; ++i )
				{
					add( __arr_for_unity[i] );
				}
				__arr_for_unity = null;
			}
		}

		[Serializable]
		public
		struct
		Array_Slice<T>
		{
			public T[] array;
			public int offset;
			public int count;

			public
			Array_Slice( T[] array) : this( array, 0, array.Length ) { }
			public static implicit operator Array_Slice<T>( T[] array ) => new Array_Slice<T>( array );
			public
			Array_Slice( T[] array, int offset, int count )
			{
				if ( offset > array.Length )
				{
					offset = array.Length;
					count = 0;
				}
				count = offset + count <= array.Length ? count : array.Length - offset;
				this.array  = array;
				this.count  = count;
				this.offset = offset;
			}
		}
		public class Pool<T> where T : class, new()
		{
			private List<T> pool = new List<T>();
			public T take()
			{
				T ret;
				if ( pool.Count == 0 )
				{
					ret = new T();
				} else
				{
					ret = pool[pool.Count-1];
					pool.RemoveAt(pool.Count-1);
				}
				return ret;
			}
		}

		public
		static
		int
		mult( params short[] arr )
		{
			int ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		uint
		mult( params ushort[] arr )
		{
			uint ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		int
		mult( params int[] arr )
		{
			int ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		uint
		mult( params uint[] arr )
		{
			uint ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		float
		mult( params float[] arr )
		{
			float ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		double
		mult( params double[] arr )
		{
			double ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		int
		sum( params short[] arr )
		{
			int ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		uint
		sum( params ushort[] arr )
		{
			uint ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		int
		sum( params int[] arr )
		{
			int ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		uint
		sum( params uint[] arr )
		{
			uint ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		float
		sum( params float[] arr )
		{
			float ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		double
		sum( params double[] arr )
		{
			double ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
	}
}