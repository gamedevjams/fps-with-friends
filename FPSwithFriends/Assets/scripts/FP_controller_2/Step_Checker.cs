﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Step_Checker : MonoBehaviour
	{
		public Rigidbody character_rb;
		public BoxCollider box;
		public bool is_free { get; private set; }
		public bool check_now()
		{
			Vector3 ext = new Vector3(
			   box.transform.lossyScale.x * box.size.x * 0.5f,
			   box.transform.lossyScale.y * box.size.y * 0.5f,
			   box.transform.lossyScale.z * box.size.z * 0.5f );
			int hit_count = Physics.OverlapBoxNonAlloc( box.transform.position, ext, hit_colliders, box.transform.rotation, 0xffffff, QueryTriggerInteraction.Ignore );
			bool was_free = is_free;
			is_free = true;
			for ( int i = 0; i != hit_count && is_free; ++i )
			{
				is_free = hit_colliders[i].attachedRigidbody == character_rb || hit_colliders[i] == box;
			}
			//log( $"hit_count = {hit_count} is_free {is_free}" );
			//if ( is_free && ! was_free )
			//{
			//	log_err( $"got free box.bounds.center = {box.bounds.center} step checker: {transform.position} rb: {character_rb.transform.position}" );
			//} else
			//{
				//log( $"ext = {ext} box.pos {box.transform.position} step checker: {transform.position} rb: {character_rb.transform.position}" );
			//}
			return is_free;
		}
		#region private
		private Collider[] hit_colliders = new Collider[256];
		private int colliders_count = 0;
		#endregion

		#region Unity
		private void Awake()
		{
			//box = GetComponent<BoxCollider>();
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		private void OnTriggerEnter( Collider other )
		{
			if ( other.attachedRigidbody == character_rb )
			{
				return;
			}

			++colliders_count;
			//is_free = false;
		}
		private void OnTriggerStay( Collider other )
		{
			if ( other.attachedRigidbody == character_rb )
			{
				return;
			}

		}
		private void OnTriggerExit( Collider other )
		{
			if ( other.attachedRigidbody == character_rb )
			{
				return;
			}

			--colliders_count;
			//is_free = colliders_count == 0;
			debug_assert( colliders_count >= 0 );
		}
		#endregion
	}
}
