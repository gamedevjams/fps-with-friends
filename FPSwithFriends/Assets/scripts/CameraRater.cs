﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	[RequireComponent( typeof( Camera ) )]
	public class CameraRater : MonoBehaviour
	{
		public bool active;
		public UnityEngine.UI.Text render_ms_text;
		public UnityEngine.UI.Text update_ms_text;
		[Min(0.01f)]
		public float Hz = 20;

		public void update_period()
		{
			period = 1.0f / Hz;
		}

		#region private
		float last_time;
		float period;
		Camera cam;
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void OnEnable()
		{
			cam = GetComponent<Camera>();
			if ( active )
			{
				cam.enabled = false;
				QualitySettings.vSyncCount = 0;
			} else
			{
				render_ms_text.text = "";
			}
			update_period();
			last_time = Time.realtimeSinceStartup;
		}
		private void Update()
		{
			update_ms_text.text = $"update {((Time.deltaTime / Time.timeScale) * 1000f).ToString("0.00"),6}ms";
			if ( active )
			{
				if ( ( cam.enabled = ( Time.realtimeSinceStartup - last_time >= period ) ) )
				{
					render_ms_text.text = $"render {((Time.realtimeSinceStartup - last_time) * 1000f).ToString("0.00"),6}ms";
					last_time = Time.realtimeSinceStartup;
					//cam.Render();
				}
			}
		}
		private void OnDisable()
		{
			QualitySettings.vSyncCount = 1;
			cam.enabled = true;
		}
		private void OnPostRender()
		{
		}
		#if UNITY_EDITOR
		private void OnValidate()
		{
			update_period();
		}
		#endif
		#endregion
	}
}
