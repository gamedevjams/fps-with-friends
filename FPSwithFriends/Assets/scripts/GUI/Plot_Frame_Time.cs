﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Plot_Frame_Time : MonoBehaviour
	{
		public Plot_CRT plot;
		[Range(0,10000)]
		public float min_ms;
		[Range(0,10000)]
		public float max_ms;
		[Range(0,1024)]
		public float pow_base = 1024f;

		#region private
		private float max_exp = 1.0f;
		private float pow_span;// = Mathf.Pow( pow_base, max_exp ) - 1.0f;
		private
		float
		lin01_to_log01( float v )
		{
			float ret = v * pow_span + 1;
			ret = Mathf.Log( ret, pow_base ) / max_exp;
			return ret;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			pow_span = Mathf.Pow( pow_base, max_exp ) - 1.0f;
		}
		private void Start()
		{
		}
		private void Update()
		{
			float val01 = ( Mathf.Clamp( Time.deltaTime * 1000f, min_ms, max_ms ) - min_ms ) / ( max_ms - min_ms );
			val01 = lin01_to_log01( val01 );
			plot.plot01( val01 );
		}
		#endregion
	}
}
