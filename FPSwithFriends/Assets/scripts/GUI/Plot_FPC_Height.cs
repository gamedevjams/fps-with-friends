﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	[RequireComponent(typeof(Plot_CRT_Ranges))]
	public class Plot_FPC_Height : MonoBehaviour
	{
		public FP_Controller fpc;

		#region private
		private Plot_CRT_Ranges plot_crt;
		private const float min_height = -1f;
		private const float max_height = 1f;
		private const float height_span = max_height - min_height;
		private
		float
		height_to_01( float height )
		{
			float ret = ( height - min_height ) / height_span;
			return ret;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			plot_crt = GetComponent<Plot_CRT_Ranges>();
			plot_crt.ranges_max[0] = height_to_01( -1 );
			plot_crt.ranges_max[1] = height_to_01( -2f / 3f );
			plot_crt.ranges_max[2] = height_to_01( -1f / 3f );
			plot_crt.ranges_max[3] = height_to_01( 0 );
			plot_crt.ranges_max[4] = height_to_01( 1f / 3f );
			plot_crt.ranges_max[5] = height_to_01( 2f / 3f );
			plot_crt.ranges_max[6] = height_to_01( 1 );
			plot_crt.fg_colors[0] = new Color(0.2588235f,0.5623817f,0.7058824f,0.8f);
			plot_crt.fg_colors[1] = new Color(0.2862745f,0.7058824f,0.2588235f,0.8f);
			plot_crt.fg_colors[2] = new Color(1f,0.9294118f,0.3098039f,0.8f);
			plot_crt.fg_colors[3] = new Color(1f,0.4666667f,0f,0.8f);
			plot_crt.fg_colors[4] = new Color(1f,0f,1f,1f);
			plot_crt.fg_colors[5] = new Color(1f,0f,0f,1f);
			plot_crt.fg_colors[6] = new Color( 0.1606444f, 0.8962264f, 0.5705467f, 1f );
		}
		private void Start()
		{
		}
		private void Update()
		{
			float h = fpc.height;
			plot_crt.plot01( height_to_01( h ) );
		}
		#endregion
	}
}
