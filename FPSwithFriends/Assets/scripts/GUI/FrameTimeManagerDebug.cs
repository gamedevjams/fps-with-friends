﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class FrameTimeManagerDebug : MonoBehaviour
	{
		public Text frame_ms;
		public Text fixed_ms;
		public Text fixedUnscTime;
		public Text root_fixu_time;
		public Text root_fixu_unsc_time;
		public Text curr_fixu_over_fixunsc;
		public Text fixunsc_diff_curr_fixu;
		public Text ratio;
		public Text delta;
		public Text delta_fixu_ms;
		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
			frame_ms              .text = FrameTimeManager.frame_time_ms        .ToString("0.000") + "ms";
			fixed_ms              .text = FrameTimeManager.curr_fixu_end_time_ms.ToString("0.000") + "ms";
			fixedUnscTime         .text = ( Time.fixedUnscaledTime * 1000f ).ToString("0.000") + "ms";
			root_fixu_time        .text = ( FrameTimeManager.root_fixed_time_sec * 1000f ).ToString("0.000") + "ms";
			root_fixu_unsc_time   .text = ( FrameTimeManager.root_fixed_unscaled_time_sec * 1000f ).ToString("0.000") + "ms";
			curr_fixu_over_fixunsc.text = ( FrameTimeManager.curr_fixu_end_time_ms / ( Time.fixedUnscaledTime * 1000f ) ).ToString("0.000");
			fixunsc_diff_curr_fixu.text = ( Time.fixedUnscaledTime * 1000f - FrameTimeManager.curr_fixu_end_time_ms ).ToString("0.000");
			ratio                 .text = ( FrameTimeManager.frame_time_ms / FrameTimeManager.curr_fixu_end_time_ms ).ToString("0.000");
			delta                 .text = ( FrameTimeManager.curr_fixu_end_time_ms - FrameTimeManager.frame_time_ms ).ToString("0.000") + "ms";
			delta_fixu_ms         .text = FrameTimeManager.delta_fixu_ms        .ToString("0.000") + "ms";
		}
		#endregion
	}
}
