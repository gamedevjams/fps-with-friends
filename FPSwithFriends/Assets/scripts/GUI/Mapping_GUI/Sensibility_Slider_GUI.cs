﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System.Globalization;
using System;

namespace tg
{
	public class Sensibility_Slider_GUI : MonoBehaviour
	{
		public Mapping_GUI              mapping_gui;
		public FP_Controller.Mouse_Hand hand;
		public int                      axis;
		public Slider                   slider;
		public TMP_Text                 label_text;
		public TMP_InputField           value_input_field;
		public float                    min_value;
		public float                    max_value;
		//public float                    default_value = 1.0f;

		#region private
		[SerializeField]
		private float pow_base = 1000.0f;
		[SerializeField]
		private float max_exp = 1.0f;
		private float pow_span;// = Mathf.Pow( pow_base, max_exp ) - 1.0f;
		private float span;

		private
		void
		value_change_handler( float val_slider )
		{
			float v = val_slider;
			v = Mathf.Pow( pow_base, v );
			v = min_value + ( v - 1.0f ) / pow_span * span;
			handle_real_value( v );
		}
		private
		void
		handle_real_value( float real_value )
		{
			float v = real_value;
			v = Mathf.Clamp( v, min_value, max_value );
			v = mapping_gui.change_sensibility( hand, axis, v );
			set_slider_v( v );
			string str = v.ToString();
			old_unvalidated_str = old_str = str;
			value_input_field.SetTextWithoutNotify( str );
		}
		private
		void
		set_slider_v( float real_value )
		{
			float v = real_value;
			v = Mathf.Clamp( v, min_value, max_value );
			v = ( v - min_value ) / span * pow_span + 1;
			v = Mathf.Log( v, pow_base );
			slider.SetValueWithoutNotify( v );
		}
		private string old_str;
		private string old_unvalidated_str;
		private
		void
		value_change_handler_str( string str )
		{
			//log( $"parsing '{str}'" );
			bool parsed = float.TryParse( str.Replace( ',', '.' ), NumberStyles.Float | NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out float val );
			if ( ! parsed )
			{
				for ( int i = 0, c = 0; i != str.Length; ++i )
				{
					if ( str[i] == '.' || str[i] == ',' )
					{
						++c;
						if ( c > 1 )
						{
							value_input_field.SetTextWithoutNotify( old_unvalidated_str );
							return;
						}
					}
				}
				old_unvalidated_str = str;
				return;
			}
			old_unvalidated_str = str;
			if ( old_str == val.ToString() || val == 0 )
			{
				return;
			}
			handle_real_value( val );
		}
		private bool input_field_focused;
		#endregion

		#region Unity
		private void Awake()
		{
			slider.onValueChanged.AddListener( value_change_handler );
			value_input_field.onValueChanged.AddListener( value_change_handler_str );
			input_field_focused = value_input_field.isFocused;
		}
		private void Start()
		{
			label_text.text = ( axis == 0 ? "Horizontal " : "Vertical " ) + label_text.text;

			max_value = Mathf.Max( min_value, max_value );
			span = max_value - min_value;
			slider.minValue = 0;
			slider.maxValue = max_exp;
			handle_real_value( mapping_gui.get_sensibility( hand, axis ) );
		}
		private void Update()
		{
			if ( ! value_input_field.isFocused && input_field_focused )
			{
				value_input_field.SetTextWithoutNotify( old_str );
			}
			input_field_focused = value_input_field.isFocused;
		}
		#if UNITY_EDITOR
		private void OnValidate()
		{
			pow_span = Mathf.Pow( pow_base, max_exp ) - 1.0f;
		}
		#endif
		#endregion
	}
}
