﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Mapping_GUI : MonoBehaviour
	{
		public FP_Controller.Mapping      mapping;
		public Mapping_GUI_Entry          entry_prefab;
		public Transform                  entries_container;
		public Remapping_Overlay_GUI      remapping_overlay;
		public Remapping_Confirmation_GUI confirmation_gui;
		public Mouse_Hand_Change_Overlay_GUI      mouse_changing_overlay;

		public Mouse_Hand_Change_Confirmation_GUI mouse_change_confirmation_gui;
		public Mouse_Hand_Entry_GUI[]             mouse_entries = new Mouse_Hand_Entry_GUI[2];

		public
		string
		mapping_text( FP_Controller.Action_Index action_index )
		{
			string ret = source_text( mapping.mapping.sources[(int)action_index] );
			return ret;
		}
		public
		string
		source_text( Game_Input.Input_Source source )
		{
			string ret = source.ToString();
			if ( source.type == Game_Input.Input_Source.Type.KEY )
			{
				if ( source.input_system == Game_Input.Input_Source.Input_System.RAW )
				{
					ret = source.scancode_index.ToString();
				} else
				{
					ret = source.key_code.ToString();
				}
			} else
			if ( source.type == Game_Input.Input_Source.Type.MOUSE_BUTTON )
			{
				ret = source.mouse_button.ToString();

				if ( source.device_path == mapping.left_mouse_path )
				{
					ret += " L";
				} else
				if ( source.device_path == mapping.right_mouse_path )
				{
					ret += " R";
				}
			}
			return ret;
		}

		public
		float
		change_sensibility( FP_Controller.Mouse_Hand hand, int axis, float v )
		{
			debug_assert( axis == 0 || axis == 1 );
			float ret = v;
			v = mapping.set_sensibility( hand, axis, v );
			return ret;
		}
		public
		float
		get_sensibility( FP_Controller.Mouse_Hand hand, int axis )
		{
			float ret = mapping.get_sensibility( hand, axis );
			return ret;
		}


		public void confirmation_cancel()
		{
			confirmation_callback( false );
			confirmation_gui.SetActive( false );
			confirmation_callback = null;
		}

		public void confirmation_confirm()
		{
			confirmation_callback( true );
			confirmation_gui.SetActive( false );
			confirmation_callback = null;
		}


		public void mouse_change_confirmation_cancel()
		{
			mouse_change_confirmation_callback( false );
			mouse_change_confirmation_gui.SetActive( false );
			mouse_change_confirmation_callback = null;
		}

		public void mouse_change_confirmation_confirm()
		{
			mouse_change_confirmation_callback( true );
			mouse_change_confirmation_gui.SetActive( false );
			mouse_change_confirmation_callback = null;
		}

		public
		string
		action_text( FP_Controller.Action_Index action_index )
		{
			string ret = action_index.ToString();
			return ret;
		}
		public
		void
		request_remap( FP_Controller.Action_Index action_index )
		{
			remapping_action_index = action_index;
			remapping_overlay.action_label_text.text = action_text( action_index );
			remapping_overlay.SetActive( true );
			mapping.remap_capture( action_index, remap_completion_handler, remap_confirmation_handler );
		}

		public
		void
		request_mouse_hand_change( FP_Controller.Mouse_Hand mouse_hand )
		{
			changing_mouse_hand = mouse_hand;
			mouse_changing_overlay.set_hand( mouse_hand );
			mouse_changing_overlay.SetActive( true );
			mapping.change_mouse_capture( mouse_hand, mouse_change_completion_handler, mouse_change_confirmation_handler );
		}

		#region private
		private FP_Controller.Action_Index remapping_action_index;
		private Action<bool>               confirmation_callback;
		private List<int>                  colliding_actions;
		private Mapping_GUI_Entry[]        entries;

		private Action<bool>               mouse_change_confirmation_callback;
		private FP_Controller.Mouse_Hand   changing_mouse_hand;

		private
		void
		remap_completion_handler( bool remapped )
		{
			remapping_overlay.SetActive( false );
			if ( remapped )
			{
				var e = entries[(int)remapping_action_index];
				e.refresh();
				if ( colliding_actions != null )
				{
					for ( int i = 0; i != colliding_actions.Count; ++i )
					{
						entries[colliding_actions[i]].refresh();
					}
				}
			}
			colliding_actions = null;
		}
		private
		void
		remap_confirmation_handler( FP_Controller.Action_Index action_index,
		                            Game_Input.Input_Source    source,
		                            List<int>                  colliding_actions,
		                            Action<bool>               confirmation_callback )
		{
			this.confirmation_callback = confirmation_callback;
			this.colliding_actions     = colliding_actions;
			confirmation_gui.action_label_text.text = action_text( action_index );
			confirmation_gui.source_text.text = source_text( source );
			string colliding_str = "";
			for ( int i = 0; i != colliding_actions.Count; ++i )
			{
				colliding_str += $"{action_text( (FP_Controller.Action_Index)colliding_actions[i] )}\n";
			}
			confirmation_gui.colliding_actions_text.text = colliding_str;
			confirmation_gui.SetActive( true );
		}

		private
		void
		mouse_change_completion_handler( bool changed )
		{
			mouse_changing_overlay.SetActive( false );
			if ( changed )
			{
				for ( int i = 0; i != mouse_entries.Length; ++i )
				{
					mouse_entries[i].refresh();
				}
			}
			mouse_change_confirmation_callback = null;
		}
		private
		void
		mouse_change_confirmation_handler( FP_Controller.Mouse_Hand changing_hand, Game_Input.Device_Path new_mouse_path, Action<bool> confirmation_callback )
		{
			this.mouse_change_confirmation_callback = confirmation_callback;
			string h = changing_hand == FP_Controller.Mouse_Hand.LEFT ? "left" : "right";
			string info_str = $"Assigning mouse for the {h} hand";
			mouse_change_confirmation_gui.info_text.text = info_str;
			mouse_change_confirmation_gui.new_mouse_info_text.text = Game_Input.instance.devices_infos[new_mouse_path].ToString();
			mouse_change_confirmation_gui.SetActive( true );
		}

		private
		void
		refresh()
		{
			for ( int i = 0, ic = entries.Length; i != ic; ++i )
			{
				entries[i].refresh();
			}
			for ( int i = 0; i != mouse_entries.Length; ++i )
			{
				mouse_entries[i].refresh();
			}
		}
		#endregion

		#region Unity
		private void Awake()
		{
			confirmation_gui.mapping_gui = this;
			mouse_change_confirmation_gui.mapping_gui = this;
			for ( int i = 0; i != mouse_entries.Length; ++i )
			{
				mouse_entries[i].set_mapping_gui( this );
				for ( int s_i = 0; s_i != mouse_entries[i].sensibility_slider_gui.Length; ++s_i )
				{
					mouse_entries[i].sensibility_slider_gui[s_i].min_value = FP_Controller.Mapping.MIN_SENSIBILITY;
					mouse_entries[i].sensibility_slider_gui[s_i].max_value = FP_Controller.Mapping.MAX_SENSIBILITY;
				}
				mouse_entries[i].set_data( (FP_Controller.Mouse_Hand)i );
			}

			FP_Controller fp_controller = FindObjectOfType<FP_Controller>();
			mapping = fp_controller.mapping;
			fp_controller.on_mapping_loaded += ( fpc, m )=>
			{
				mapping = m;
				refresh();
			};
		}
		private void Start()
		{

			for ( int i = 0; i != mouse_entries.Length; ++i )
			{
				mouse_entries[i].refresh();
			}


			entries = new Mapping_GUI_Entry[Enum.GetValues(typeof(FP_Controller.Action_Index)).Length];
			for ( int i = 0, ic = entries.Length; i != ic; ++i )
			{
				FP_Controller.Action_Index action_index = (FP_Controller.Action_Index)i;
				Mapping_GUI_Entry entry = Instantiate( entry_prefab, entries_container );
				entry.mapping_gui = this;
				entry.set_action_index( action_index );
				entries[i] = entry;
			}

			confirmation_gui             .gameObject.SetActive( false );
			remapping_overlay            .gameObject.SetActive( false );
			mouse_change_confirmation_gui.gameObject.SetActive( false );
			mouse_changing_overlay       .gameObject.SetActive( false );
		}
		private void Update()
		{
		}
		#endregion
	}
}
