Shader "tg/debug/Collider"
{
    Properties
    {
        //_Color ("Color", Color) = (1,1,1,1)
        //_MainTex ("Albedo (RGB)", 2D) = "white" {}
        _AlphaMultiplier( "Alpha Multiplier", Float ) = 1
        //_Glossiness ("Smoothness", Range(0,1)) = 0.5
        //_Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 200

        Pass
        {
            ZWrite Off
            ZTest LEqual
            Cull Back
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma target 4.5
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            //#pragma multi_compile_fog
            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
                uint instanceID : SV_InstanceID;
                //float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                //float2 uv : TEXCOORD0;
                //UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD1;
                float4 color  : TEXCOORD0;
                float3 v_vertex : TEXCOORD2;
            };

            //sampler2D _MainTex;
            //float4 _MainTex_ST;
            StructuredBuffer<float4> instance_color : register(t0);
            half _AlphaMultiplier;

            // ACES tone mapping curve fit to go from HDR to LDR
            //https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
            float3 ACESFilm(float3 x)
            {
                float a = 2.51;
                float b = 0.03;
                float c = 2.43;
                float d = 0.59;
                float e = 0.14;
                return clamp((x*(a*x + b)) / (x*(c*x + d) + e), 0.0, 1.0);
            }
            float3 LessThan(float3 f, float value)
            {
                return float3(
                    (f.x < value) ? 1.0 : 0.0,
                    (f.y < value) ? 1.0 : 0.0,
                    (f.z < value) ? 1.0 : 0.0);
            }
            float3 LinearToSRGB(float3 rgb)
            {
                rgb = clamp(rgb, 0.0, 1.0);

                float f = 1.0/2.4;
                return lerp(
                    pow(rgb, float3(f,f,f) ) * 1.055 - 0.055,
                    rgb * 12.92,
                    LessThan(rgb, 0.0031308)
                );
            }

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.v_vertex = UnityObjectToViewPos( v.vertex );
                float3 wnormal = UnityObjectToWorldNormal(v.normal);
                float3 vnormal = mul( (float3x3)UNITY_MATRIX_V, wnormal );
                o.normal = vnormal;
                o.color = instance_color[v.instanceID];
                //o.color = fixed4(0,1,0,1);
                //o.color = float4(v.instanceID,0,0,1);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //UNITY_TRANSFER_FOG(o,o.vertex);
                //UNITY_TRANSFER_INSTANCE_ID(v, o);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                fixed4 col = i.color;
                fixed3 ncol = (fixed3)i.normal * 0.5 + 0.5;

                float ambient = 0.1;
                float l = ambient + max( 0, dot( i.normal, - normalize( i.v_vertex - float3(0.5,0.866,0) ) ) );
                //l = l + max( 0, dot( i.normal, - normalize( -float3(0.5,0.866,0.5) ) ) );
                float3 _rgb = l * ( 0.2 + i.color.rgb );
                _rgb = ACESFilm( _rgb );
                col.rgb = LinearToSRGB( _rgb );

                //col.rgb += dot( ncol.xy, ncol.xy );
                //col.rgb = lerp( ncol, col.rgb, 0.5 );
                //col.rgb = ncol;

                //col.a = max( col.a, 0.5 );
                //col.a = saturate( i.color.a * _AlphaMultiplier * ( 0.1 + length(i.normal.xy) ) );
                col.a = saturate( i.color.a * _AlphaMultiplier );

                return col;
            }
            ENDCG
        }
    }
}
